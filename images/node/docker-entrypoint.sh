#!/bin/sh

update-ca-certificates

npm config set proxy http://33.3.3.254:3128/
npm config set https-proxy http://33.3.3.254:3128/
npm config set cafile /usr/local/share/ca-certificates/pgd.crt

npm install -g pm2 bower gulp http-server knex --loglevel error

cd /usr/src/app/server 
npm install --loglevel error
knex migrate:latest

cd /usr/src/app/client
npm install --loglevel error
bower install --allow-root -s
gulp

cd /usr/src/app/ 
pm2 start ecosystem.config.js
pm2 logs
