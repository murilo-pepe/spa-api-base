# Objetivo
    Servir como ponto inicial para criação de aplicativos com front em Angular e back em Node

# Arquitetura
    O sistema foi pensado para ter o backend e o frontend completamente 
    desacoplados, comunicando apenas através de api rest

## Client
    Responsável pela parte visual do app, foi implementado usando 
    Angular e seguindo o style guide acessível em 
    https://github.com/johnpapa/angular-styleguide/tree/master/a1

## Server
    Responsável por coletar e disponibilizar todas os serviços para a camada
    de visualização. Foi utilizado o framework express para criação da 
    API. Para acesso ao banco de dados, foi utilizada 
    a biblioteca Knex (http://knexjs.org/) a qual poderá ser extendida para 
    uso com ORM Bookshelf caso desejado (http://bookshelfjs.org/). 
    Para desenvolvimento com TDD foi utilizada a ferramenta Mocha 
    (https://mochajs.org/) para execução e a ferramenta Should 
    (https://shouldjs.github.io/) para asserts em estilo BDD

### bibliotecas 
    - BottleJs: DependencyInjection > Abstraída pelo script module/core/libs/dependencyInjection.js, acessível pela global "di"
    - Knex: Query builder > Abstraído pelo script module/core/libs/database.js, acessível por "di.get('database')"
    - Bookshelf: ORM > Abstraído pelo script module/core/libs/orm.js, acessível por "di.get('orm')"
    - Express: Framework web > Acessível por "di.get('app')"
    - Lodash: Utilitários > Acessível pela global "_"
    - Moment: Biblioteca para controle de data/hora > Acessível pela gobal "moment"

### Componentes
    Todo o server foi pensado utilizando serviços, permitindo que seja desacoplado da camada execução
    (HTTP, daemon ou qualquer outra). Sendo assim, todo os módulos de negócio ficam dentro da pasta
    "modules" e NÃO DEVEM depender de compoentes externos á essa pasta.

    Caso um script externo deseje utilizar as bibliotecas de "modules", este deverá incluir o arquivo 
    de inicialização "modules/bootstrap.js", fazendo com que todos os componentes tornem-se disponíveis
    pela camada de Dependency Injection através do comando di.get(nome_do_componente)

#### Dependency Injection   
    Todas as bibliotecas de modules foram pensadas de forma a utilizar injeção de dependencias
    Foi implementado um Façade para a bibliteca pública BottleJs (https://github.com/young-steveo/bottlejs)
    em libs/dependencyInjection, sendo que ao realizar o bootsrap, uma instância desse componente torna-se
    disponível na variável global "di".

    Exemplo:

        // "modules/batata/batata.service.js"
        di.service('batataService', batataService); //registra o serviço

        function batataService(){
            return {
                getBatata: function(){
                    return 'batata';
                }
            }
        }

        // "teste.js"
        require('modules/bootstrap.js');
        di.get('batataService').getBatata(); //retorna e utiliza um serviço

    
    Para maiores detalhes vide arquivo dependencyInjection.js        

## Documentação da API
    É gerada automaticamente a partir dos jsdocs. 
    Utilize os padrões encontrados em https://github.com/Surnet/swagger-jsdoc para gerar os docs.
    Interface pode ser acessada através do link http://localhost:4040/api-docs

# Versionamento
    Seguir padrão GitFlow (http://nvie.com/posts/a-successful-git-branching-model/ 
    e https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

# Pre-requisitos    
    - Docker instalado
    - Docker composer instalado

    $ apt-get install docker docker-compose

# Instalação
    Na raiz do projeto

    Defina o ambiente criando um arquivo .env com as variáveis ENV (define o ambiente), API_PORT, WEB_PORT e DB_PORT (definem as portas que o docker utilizará na maq host)
    $ echo "ENV=[dev, prod ou stage]" > .env
    $ echo "API_PORT=4040" >> .env
    $ echo "WEB_PORT=8080" >> .env
    $ echo "DB_PORT=3306" >> .env
    
    Inicie os containers do sistema    
    $ docker-compose up
    
    Os serviços serão automaticamente iniciados nas portas 4040 (api) e 8080 (front)
    Para testar, abra um navegador e acesse localhost:8080

## Populando a base de dados    
    Caso deseje popular a base de dados com registros predefinidos, acesso o container web e execute o script seed

    $ docker ps 

    Verifique o nome do container web e acesse-o

    $ docker exec -it <container> /bin/bash
    $ cd server
    $ knex seed:run
