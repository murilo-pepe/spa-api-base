env = process.env.NODE_ENV || 'dev'

////////////// Greetings ////////////////////////
var out = require('./server/node_modules/figlet')
var chalk = require('./server/node_modules/chalk')
var boxen = require('./server/node_modules/boxen')

function p(text, color, font) {
    return (color(out.textSync(text, {
        font: font
    })))
}

console.log(boxen(''.concat(p('  PG+', chalk.green, 'Graffiti'),
    '\n',
    p('ScafNode', chalk.black, 'Small'),
    '\n\n\n',
    p(env.toUpperCase(), chalk.red, 'Doom')
), {
    align: 'center',
    borderStyle: 'round',
    margin: 5,
    padding: 2,
    dimBorder: true,
    backgroundColor: 'white',
}))
/////////////////////////////////////////////////


module.exports = {
    apps: [

        // Api
        {
            name: "api-" + env,
            script: "server/http/run.js",
            watch: ['server/'],
        },

        // FrontEnd
        {
            name: "front-" + env,
            script: "web/dist",
            watch: false,
            interpreter: 'http-server',
            args: '-p 8080',
        },

    ]
}