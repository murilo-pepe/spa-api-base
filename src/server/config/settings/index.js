(function () {
    'use strict';

    var globalSettingsFile = require('./settings.global.js')
    var envSettingsFile = require('./settings.' + (global.env || process.env.NODE_ENV || 'dev'))


    di.constant('settings', _.merge(globalSettingsFile, envSettingsFile))

})()