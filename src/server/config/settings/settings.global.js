var defaultPoolConf = {
    min: 0,
    max: 14,
    idleTimeout: 10 * 1000,
    syncInterval: 10 * 1000,
    maxRequests: Infinity,
    requestTimeout: Infinity,
    ping: function(conn, cb) {
        conn.query('SELECT 1', cb);
    },
    afterCreate: function(connection, callback) {
        connection.query("set time_zone='America/Sao_Paulo';", function(err) {
            callback(err, connection);
        });
    }
}

module.exports = {
    http_port: 4040,
    //Definie se será utilizado middleware para logar requests - atualmente usando "morgan"
    request_logger: {
        log: true,
        style: 'dev',
        options: {}
    },
    //Define conexões com bases de dados
    database: {
        default: 'default',
        connections: {
            default: {
                //debug: true,
                client: 'mysql',
                connection: {
                    multipleStatements: true,
                    database: 'dashboard',
                    host: 'dbhost',
                    port: '3306',
                    user: 'root',
                    password: 'sorvete123'
                },
                acquireConnectionTimeout: 60 * 1000,
                pool: defaultPoolConf
            },
         
        },
    }

};