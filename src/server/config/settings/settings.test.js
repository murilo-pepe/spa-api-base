module.exports = {
    database: {
        connections: {
            default: {
                //debug: true,
                client: 'sqlite3',
                connection: {
                    filename: ':memory:'
                },
                useNullAsDefault: true,
                acquireConnectionTimeout: null,
                pool: null
            },
        },
    }
};