(function () {
    'use strict';

    //Carrega bibliotecas vendor
    var express = require('express');
    var bodyParser = require('body-parser');
    var logger = require('morgan');
    var TokenStrategy = require('passport-accesstoken').Strategy;
    var passport = require('passport');
    var path = require('path');

    //Registra App no injetor de dependências
    di.service('app', express)

    //Carrega bibliotecas locais
    var settings = di.get('settings')
    var tokenService = di.get('tokenService');
    var usuarioService = di.get('usuariosService');
    var app = di.get('app');

    //Faz o parse de do corpo da requisição
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: false
    }));

    //Serve os arquivos estáticos da pasta public
    app.use(express.static(path.join(__dirname, 'public')));

    //Define o logger de requisiçoes
    if (settings.request_logger.log) {
        app.use(logger(settings.request_logger.style, settings.request_logger.options));
    }

    //Define a estratégia de autenticação
    passport.use(new TokenStrategy(
        function (token, done) {
            tokenService
                .buscarToken(token)
                .then(function (token) {
                    return usuarioService
                        .buscarUsuarioPorId(token.idUsuario);
                })
                .then(function (usuario) {
                    return done(null, usuario);
                })
                .catch(function (err) {
                    return done(err);
                });
        }
    ));

    //Inicia o servidor web
    app.listen(settings.http_port, function () {
        console.log(('Servidor http inciado na porta ' + settings.http_port).green);
    })

    //Registra os módulos 
    registerModule('http/middlewares')
    registerModule('http/routes')

    // Caso nenhuma rota registrada até aqui satisfaça o request
    // passa um erro de rota não econtrada para frente
    app.use(function (req, res, next) {
        next(di.get('errorProvider').rotaNaoEncontrada());
    });

    // Tratamento de erros
    app.use(function (err, req, res, next) {
        var error = di.get('errorHandler').normalize(err);
        res.status(error.status)
            .json(error);
    });

})()