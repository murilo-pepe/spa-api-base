(function () {
    'use strict';

    var passport = require('passport');

    di.service('authenticationMiddleware', authenticationMiddleware)

    function authenticationMiddleware() {
        return passport.authenticate('token', {
            session: false
        })
    }

})()