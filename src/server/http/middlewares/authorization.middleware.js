(function () {
    'use strict';

    di.service('authorizationMiddleware', authorizationMiddleware, 'aclService', 'errorProvider')

    function authorizationMiddleware(aclService, errorProvider) {

        return function checkPermissaoUsuarioFromRoute(req, res, next) {
            var identificador_usuario = req.user.email
            var recurso = req.route.path
            var permissao = (req.method).toLowerCase()

            aclService
                .isPermitido(identificador_usuario, recurso, permissao)
                .then(function (isAllowed) {
                    if (!isAllowed) {
                        return next(errorProvider.naoAutorizado())
                    }
                    return next()
                })
        }



    }

})()