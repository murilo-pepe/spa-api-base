(function () {
    'use strict';

    var app = di.get('app');
    var errorProvider = di.get('errorProvider');

    /**
     * @swagger
     * /auth/login:
     *   post:
     *     tags:
     *       - Autenticação
     *     summary: Realiza login e obtem token
     *     description: Obtem token através do login utilizando email e senha de um usuário válido
     *     parameters:
     *       - name: email
     *         description: Email do usuário
     *         in: formData
     *         required: true
     *         type: string
     *       - name: senha
     *         description: Senha do usuário
     *         in: formData
     *         required: true
     *         type: string
     */
    app.post('/auth/login', function (req, res, next) {

        var usuarioService = di.get('usuariosService')

        usuarioService
            .buscarUsuarioPorEmail(req.body.email)
            .then(function (usuario) {

                if (!usuarioService.validarHash(req.body.senha, usuario.salt, usuario.senha)) {
                    throw errorProvider.formularioInvalido('E-mail e senha informados não coincidem');
                }

                if (usuario.bloqueado) {
                    throw errorProvider.usuarioBloqueado(usuario);
                }

                di.get('tokenService')
                    .gerarToken(usuario.id)
                    .then(function (token) {
                        res.send({
                            token: token,
                            usuario: {
                                email: usuario.email,
                                nome: usuario.nome
                            }
                        });
                    });
            })
            .catch(function (err) {
                next(err)
            });
    });



    /**
     * @swagger
     * /auth/token/{token}/user:
     *   get:
     *     tags:
     *       - Autenticação
     *     summary: Busca o usuário de um token
     *     description: Obtem o usuário atrelado à um token
     *     parameters:
     *       - name: token
     *         description: Token
     *         in: path
     *         required: true
     *         type: string
     */
    app.get('/auth/token/:token/user', function (req, res, next) {

        di.get('tokenService')
            .buscarToken(req.params.token)
            .then(function (token) {
                return di.get('usuariosService')
                    .buscarUsuarioPorId(token.idUsuario)
            })
            .then(function (usuario) {
                res.send({
                    email: usuario.email,
                    nome: usuario.nome
                });
            })
            .catch(function (err) {
                next(err)
            });
    });



    /**
     * @swagger
     * /auth/is_allowed/{resource}/{permission}:
     *   get:
     *     tags:
     *       - Autenticação
     *     summary: Verifica permissão de um recurso para o token
     *     description: Verifica se o usuário possui permissão pra o rescurso solicitado
     *     parameters:
     *       - name: x-token
     *         description: token
     *         in: header
     *         required: true
     *         type: string
     *       - name: resource
     *         description: Recurso
     *         in: path
     *         required: true
     *         type: string     
     *       - name: permission
     *         description: permissao
     *         in: path
     *         required: true
     *         type: string
     * 
     */
    app.get('/auth/is_allowed/:resource/:permission',
        di.get('authenticationMiddleware'),
        function (req, res, next) {
            di.get('aclService')
                .isPermitido(req.user.email, req.params.resource, req.params.permission)
                .then(function (isAllowed) {
                    res.send(isAllowed)
                })
                .catch(function (err) {
                    next(err)
                });
        })


})()