(function () {
    'use strict';

    var swaggerJSDoc = require('swagger-jsdoc');

    var options = {
        swaggerDefinition: {
            info: {
                title: 'API PG+ Resultado', // Title (required)
                version: '1.0.0', // Version (required)
            },
        },
        apis: ['./server/http/**/*.js'], // Path to the API docs
    };



    di.get('app')
        .get('/api/docs', function (req, res) {
            var swaggerSpec = swaggerJSDoc(options);
            res.send(swaggerSpec);
        });

})();