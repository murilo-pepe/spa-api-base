(function () {
    'use strict';

    //Bootstrap geral
    require('../modules/bootstrap');

    //Bootstrap do servidor HTTP
    require('./http.bootstrap');

})()