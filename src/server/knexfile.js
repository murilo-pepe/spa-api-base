require('./modules/bootstrap')

module.exports = {
    dev: require('./config/settings/settings.global').database.connections.default,
    development: require('./config/settings/settings.global').database.connections.default,
    stage: require('./config/settings/settings.global').database.connections.default,
    test: require('./config/settings/settings.global').database.connections.default,
    prod: require('./config/settings/settings.global').database.connections.default
};