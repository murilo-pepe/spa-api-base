exports.up = function (knex, Promise) {
    return knex.schema
            .createTable('usuarios', function (table) {

                table.increments();

                table.string('nome', 100)
                        .notNullable();

                table.string('email', 100)
                        .notNullable()
                        .unique();

                table.string('senha', 40)
                        .notNullable();

                table.integer('salt')
                        .unsigned()
                        .notNullable();

                table.timestamp('created_at', true)
                        .notNullable().defaultTo(knex.fn.now());

                table.timestamp('updated_at', true).nullable();

            });
};

exports.down = function (knex, Promise) {
    return knex.schema
            .dropTable('usuarios');
};
