exports.up = function (knex, Promise) {
    return knex.schema
        .createTable('tokens', function (table) {

            table
                .increments();

            table.string('token', 40)
                .notNullable()
                .unique();

            table.integer('idUsuario')
                .unsigned()
                .references('id')
                .inTable('usuarios')
                .notNullable();

            table.timestamp('created_at', true)
                .notNullable().defaultTo(knex.fn.now());

            table.timestamp('expire_at', true).nullable();



        });
};

exports.down = function (knex, Promise) {
    return knex.schema
        .dropTable('tokens');
};