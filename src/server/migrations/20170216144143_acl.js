var acl = require('../modules/libs/acl-knex-driver/knex-backend')
exports.up = function (knex, Promise) {
    return new Promise(function (resolve, reject) {
        new acl(knex).setup([
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            knex,
            null
        ], function () {
            return resolve()
        })
    });

};

exports.down = function (knex, Promise) {
    return new Promise(function (resolve, reject) {
        new acl(knex).teardown([
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            knex,
            null
        ], function () {
            return resolve()
        })
    });

};