(function () {
    'use strict';

    di.service('tokenModel', tokenModel, 'orm', 'usuarioModel')

    function tokenModel(orm, usuarioModel) {
        var db = orm.conn();

        return db.Model.extend({
            tableName: 'tokens',
            usuario: function () {
                return this.belongsTo(usuarioModel, 'idUsuario')
            }
        })
    }

})();