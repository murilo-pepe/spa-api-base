(function () {
    'use strict';

    di.service('usuarioModel', usuarioModel, 'orm')

    function usuarioModel(orm) {
        var db = orm.conn();

        return db.Model.extend({
            tableName: 'usuarios',
        })

    }

})();