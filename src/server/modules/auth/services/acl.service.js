(function () {
    'use strict';

    di.service('aclService', aclService, 'database')

    function aclService(database) {

        var Acl = require('acl');
        var AclKnexBackend = appRequire('libs/acl-knex-driver');
        var acl = new Acl(new AclKnexBackend(database.conn(), 'mysql', 'acl_'));


        /**
         * Adiciona permissão(ões) de recurso(s) á um ou mais papeis
         * 
         * @param {string | array} papeis
         * @param {string | array} recursos
         * @param {string | array} permissoes
         * @returns Promise
         */
        function permitir(papeis, recursos, permissoes) {
            return acl.allow([{
                roles: papeis,
                allows: [{
                    resources: recursos,
                    permissions: permissoes
                }]
            }])
        }

        /**
         * Verifica se um usuário possui permissão de um recurso
         * 
         * @param {string | array} usuario
         * @param {string | array} recurso
         * @param {string | array} permissao
         * @returns Promise
         */
        function isPermitido(usuario, recurso, permissao) {
            return acl.isAllowed(usuario, recurso, permissao)
        }

        /**
         * Atribui um papel á um usuário
         * 
         * @param {string | array} usuario
         * @param {string | array} papeis
         * @returns 
         */
        function atribuirPapel(usuario, papeis) {
            return acl.addUserRoles(usuario, papeis)
        }

        /**
         * Recupera as permissões de um usuário
         * 
         * @param {string | array} usuario
         * @param {string | array} recurso
         * @returns 
         */
        function getPermissoes(usuario, recurso) {
            return new Promise(function (resolve, reject) {
                acl.allowedPermissions(usuario, recurso, function (err, permissoes) {
                    if (err) {
                        return reject(err)
                    }
                    return resolve(permissoes)
                })
            });

        }

        return {
            permitir: permitir,
            isPermitido: isPermitido,
            atribuirPapel: atribuirPapel,
            getPermissoes: getPermissoes
        };
    }

})()