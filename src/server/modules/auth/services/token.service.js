(function () {
    'use strict';


    di.service('tokenService', tokenService, 'database', 'errorProvider')

    function tokenService(database, errorProvider) {


        var conn = database.conn();
        var uuid = require('uuid');
        var sha1 = require('sha1');

        return {
            //Cria uma nova transação e retorna seu id
            gerarToken: function (idUsuario) {
                return new Promise(function (resolve, reject) {

                    var token = {
                        token: sha1(uuid.v1()),
                        idUsuario: idUsuario,
                        expire_at: moment().add(1, 'd').toDate(),
                        created_at: moment().toDate()
                    };

                    conn
                        .insert(token)
                        .into('tokens')
                        .then(function (idCollection) {
                            return resolve(token.token);
                        })
                        .catch(function (err) {
                            return reject(err);
                        });

                });
            },
            buscarToken: function (token) {
                return new Promise(function (resolve, reject) {
                    conn('tokens')
                        .where({
                            token: token
                        })
                        .then(function (tokens) {
                            if (tokens.length === 0) {
                                return reject(errorProvider.tokenInvalido());
                            } else {
                                return resolve(tokens[0]);
                            }
                        });
                });
            }
        };
    }

})();