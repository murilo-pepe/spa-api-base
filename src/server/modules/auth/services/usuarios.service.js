(function () {
    'use strict';

    di.service('usuariosService', usuariosService, 'database', 'joiSettings', 'errorProvider');

    function usuariosService(database, joiSettings, errorProvider) {

        var conn = database.conn();
        var joi = require('joi');
        var sha1 = require('sha1');
        var fs = require('fs');

        var usuarioSchema = joi.object().keys({
            nome: joi.string().regex(/^[a-zA-Z0-9 ]*$/).min(3).max(100).required(),
            senha: joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
            email: joi.string().max(100).email().required()
        });

        function gerarHash(param1, param2) {
            return sha1(param1 + '-' + param2);
        }

        var srvUsuarios = {
            //Retorna um único usuário dado um id
            buscarUsuarioPorId: function (id) {
                return new Promise(function (resolve, reject) {
                    conn('usuarios')
                        .where({
                            id: id
                        })
                        .then(function (usuarios) {
                            return resolve(usuarios[0]);
                        });
                });
            },
            //Retorna um único usuário dado um id
            buscarUsuarioPorEmail: function (email) {

                return new Promise(function (resolve, reject) {

                    if (!_.isString(email)) {
                        return reject(errorProvider.formularioInvalido('E-mail inválido'));
                    }

                    conn('usuarios')
                        .where({
                            email: email
                        })
                        .then(function (usuarios) {
                            if (usuarios.length === 0) {
                                return reject(errorProvider.naoEncontrado('E-mail não encontrado'));
                            } else {
                                return resolve(usuarios[0]);
                            }
                        });
                });


            },
            validarHash: function (param1, param2, hash) {
                return sha1(param1 + '-' + param2) === hash;
            },
            //Cria uma nova transação e retorna seu id
            criarUsuario: function (obj) {
                return new Promise(function (resolve, reject) {
                    var usuario = null;

                    var validatedObj = joi.validate(
                        obj,
                        usuarioSchema,
                        joiSettings
                    );

                    if (validatedObj.error !== null) {
                        return reject(validatedObj.error);
                    } else {
                        usuario = validatedObj.value;
                    }

                    usuario.salt = _.random(2147483646);
                    usuario.senha = gerarHash(usuario.senha, usuario.salt);
                    conn
                        .insert(validatedObj.value)
                        .into('usuarios')
                        .then(function (idCollection) {
                            usuario.id = idCollection[0];
                            srvUsuarios
                                .criarTokenValidacaoCadastro(usuario)
                                .then(function () {
                                    return resolve(usuario.id);
                                });
                        })
                        .catch(function (err) {
                            return reject('Não foi possível criar a conta solicitada, verifique se este e-mail já possui cadastro');
                        });

                });
            }
        };

        return srvUsuarios;
    }

})()