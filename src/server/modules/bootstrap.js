//////////////////////////////////////////
/*
                Greetings
*/
//////////////////////////////////////////
require('colors');
global.env = process.env.NODE_ENV || 'dev';
console.log(('Iniciando em ambiente ').yellow + (global.env).cyan);


//////////////////////////////////////////
/*
                Funções 
*/
//////////////////////////////////////////
function appRequire(module) {
    return require(__dirname + '/' + module);
};

function registerModule(module) {
    require('require-all')({
        dirname: __dirname + '/../' + module,
        excludeDirs: /^\.(git|svn)$/,
        recursive: true
    })
}



//////////////////////////////////////////
/*
            Variáveis Globais
*/
//////////////////////////////////////////
global._ = require('lodash');
global.moment = require('moment-timezone')
global.di = appRequire('core/libs/dependencyInjection')
global.appRequire = appRequire
global.registerModule = registerModule


//////////////////////////////////////////
/*
            Registro de módulos
*/
//////////////////////////////////////////
registerModule('modules/core')
registerModule('modules/auth')
registerModule('config')


//////////////////////////////////////////
/*
                Ambiente
*/
//////////////////////////////////////////
moment.tz.setDefault("America/Sao_paulo");


