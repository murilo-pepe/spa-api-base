(function () {
    'use strict';

    di.service('errorHandler', errorHandler)

    function errorHandler() {
        function normalize(prevError) {

            var err = {}
            err.message = prevError.message
            err.status = prevError.code || 400;
            if(env == 'dev'){
                err.stack = prevError.stack
            }

            return err
        }

        return {
            normalize: normalize
        };
    }

}());