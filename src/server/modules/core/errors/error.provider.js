(function () {
    'use strict';

    di.service('errorProvider', errorProvider)

    function errorProvider() {
        return {
            ///////////////////////////////////////////
            /*
                    Formulários e campos
            */
            ///////////////////////////////////////////
            formularioInvalido: function (msg) {
                var err = new Error(msg)
                err.code = 403;
                return err
            },


            ///////////////////////////////////////////
            /*
                        Comandos e Queries 
            */
            ///////////////////////////////////////////
            naoEncontrado: function (msg) {
                var err = new Error(msg)
                err.code = 403;
                return err
            },


            ///////////////////////////////////////////
            /*
                    Autenticação e autorização
            */
            ///////////////////////////////////////////
            naoAutorizado: function () {
                var err = new Error('Usuário não possui permisão para este recurso')
                err.code = 403;
                return err
            },

            usuarioBloqueado: function (usuario) {
                var err = new Error(usuario.motivoBloqueio)
                err.code = 403;
                return err
            },

            tokenInvalido: function () {
                var err = new Error("Token informado não é válido")
                err.code = 403;
                return err
            },


            ///////////////////////////////////////////
            /*
                       Requisições e rotas
            */
            //////////////////////////////////////////
            rotaNaoEncontrada: function (usuario) {
                var err = new Error('Rota não encontrada')
                err.code = 404;
                return err
            },
        }
    }


})();