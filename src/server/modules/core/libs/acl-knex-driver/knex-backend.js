/**
	Knex Backend.
	Implementation of the storage backend using Knex.js
*/
'use strict';

var contract = require('./contract');
var async = require('async');
var _ = require('lodash');
var createTables = require('./databaseTasks').createTables;
var buckets = require('./buckets');

function KnexDBBackend(db, client, prefix, options) {
    this.db = db;
    this.buckets = buckets(options);
    this.prefix = typeof prefix !== 'undefined' ? prefix : '';
}

KnexDBBackend.prototype = {
    /**  
    	 Begins a transaction.
    */
    begin: function () {
        // returns a transaction object
        return [];
    },

    /**
    	 Ends a transaction (and executes it)
    */
    end: function (transaction, cb) {
        contract(arguments)
            .params('array', 'function')
            .end();

        // Execute transaction
        async.series(transaction, function (err) {
            cb(err instanceof Error ? err : undefined);
        });
    },

    /**
    	Cleans the whole storage.
    */
    clean: function (cb) {
        contract(arguments)
            .params('function')
            .end();
        cb(undefined);
    },

    /**
    	 Gets the contents at the bucket's key.
    */
    get: function (bucket, key, cb) {
        contract(arguments)
            .params('string', 'string|number', 'function')
            .end();
        var table = '';
        if (bucket.indexOf('allows') != -1) {
            table = this.prefix + this.buckets.permissions;
            var query = this.db
                .select('key', 'value')
                .from(table)
                .where({
                    'key': bucket
                })


            query.then(function (result) {
                if (result.length) {
                    result[0].value = JSON.parse(result[0].value)
                    cb(undefined, (result[0].value[key] ? result[0].value[key] : []));
                } else {
                    cb(undefined, []);
                }
            }).catch(function (e) {
                console.trace(e)
            })

            ;
        } else {

            table = this.prefix + bucket;
            var query = this.db
                .select('key', 'value')
                .from(table)
                .where({
                    'key': key
                })


            query.then(function (result) {

                cb(undefined, (result.length ? JSON.parse(result[0].value) : []));
            }).catch(function (e) {
                console.trace(e)
            });
        }
    },

    /**
    	Returns the union of the values in the given keys.
    */
    union: function (bucket, keys, cb) {
        contract(arguments)
            .params('string', 'array', 'function')
            .end();

        var table = '';
        if (bucket.indexOf('allows') != -1) {
            table = this.prefix + this.buckets.permissions;

            var query = this.db
                .select('key', 'value')
                .from(table)
                .where({
                    'key': bucket
                })



            query.then(function (results) {
                if (results.length && results[0].value) {
                    results[0].value = JSON.parse(results[0].value)

                    var keyArrays = [];
                    _.each(keys, function (key) {
                        keyArrays.push.apply(keyArrays, results[0].value[key]);
                    });
                    cb(undefined, _.union(keyArrays));
                } else {
                    cb(undefined, []);
                }

            }).catch(function (e) {
                console.trace(e)
            });
        } else {
            table = this.prefix + bucket;
            var query = this.db
                .select('key', 'value')
                .from(table)
                .whereIn('key', keys)

            query.then(function (results) {
                if (results.length) {
                    var keyArrays = [];
                    _.each(results, function (result) {
                        keyArrays.push.apply(keyArrays, JSON.parse(result.value));
                    });
                    cb(undefined, _.union(keyArrays));
                } else {
                    cb(undefined, []);
                }
            }).catch(function (e) {
                console.trace(e)
            });
        }
    },

    /**
    	Adds values to a given key inside a table.
    */
    add: function (transaction, bucket, key, values) {
        contract(arguments)
            .params('array', 'string', 'string|number', 'string|array|number')
            .end();

        var self = this;
        var table = '';
        values = Array.isArray(values) ? values : [values]; // we always want to have an array for values

        transaction.push(function (cb) {

            if (bucket.indexOf('allows') != -1) {
                table = self.prefix + self.buckets.permissions;
                var query = self.db
                    .select('key', 'value')
                    .from(table)
                    .where({
                        'key': bucket
                    })

                query
                    .then(function (result) {
                        var json = {};
                        if (result.length === 0) {
                            json[key] = values;

                            var qIns = self.db(table)
                                .insert({
                                    key: bucket,
                                    value: JSON.stringify(json)
                                });


                            return qIns
                        } else {

                            result[0].value = JSON.parse(result[0].value)
                            // if we have found the key in the table then lets refresh the data
                            if (_.has(result[0].value, key)) {
                                result[0].value[key] = _.union(values, result[0].value[key]);
                            } else {
                                result[0].value[key] = values;
                            }

                            var qUp = self.db(table)
                                .where({
                                    'key': bucket
                                })
                                .update({
                                    key: bucket,
                                    value: JSON.stringify(result[0].value)
                                });

                            return qUp
                        }
                    }).catch(function (e) {
                        console.trace(e)
                    })




                query.then(function () {
                    cb(undefined);
                }).catch(function (e) {
                    console.trace(e)
                });
            } else {
                table = self.prefix + bucket;

                var query = self.db
                    .select('key', 'value')
                    .from(table)
                    .where({
                        'key': key
                    })


                query
                    .then(function (result) {
                        if (result.length === 0) {


                            // if no results found do a fresh insert
                            return self.db(table)
                                .insert({
                                    key: key,
                                    value: JSON.stringify(values)
                                });
                        } else {
                            result[0].value = JSON.parse(result[0].value)
                            // if we have found the key in the table then lets refresh the data
                            var upq = self.db(table)
                                .where('key', key)
                                .update({
                                    value: JSON.stringify(_.union(values, result[0].value))
                                });


                            return upq
                        }
                    }).catch(function (e) {
                        console.trace(e)
                    })

                query.then(function () {
                    cb(undefined);
                }).catch(function (e) {
                    console.trace(e.code)
                });
            }
        });
    },

    /**
    	 Delete the given key(s) at the bucket
    */
    del: function (transaction, bucket, keys) {
        contract(arguments)
            .params('array', 'string', 'string|array')
            .end();

        var self = this;
        var table = '';
        keys = Array.isArray(keys) ? keys : [keys]; // we always want to have an array for keys

        transaction.push(function (cb) {

            if (bucket.indexOf('allows') != -1) {
                table = self.prefix + self.buckets.permissions;
                var query = self.db
                    .select('key', 'value')
                    .from(table)
                    .where({
                        'key': bucket
                    })



                query.then(function (result) {
                        if (result.length === 0) {

                        } else {
                            result[0].value = JSON.parse(result[0].value)
                            _.each(keys, function (value) {
                                result[0].value = _.omit(result[0].value, value);
                            });

                            if (_.isEmpty(result[0].value)) {
                                // if no more roles stored for a resource the remove the resource
                                return self.db(table)
                                    .where({
                                        'key': bucket
                                    })
                                    .del();
                            } else {
                                return self.db(table)
                                    .where({
                                        'key': bucket
                                    })
                                    .update({
                                        value: result[0].value
                                    });
                            }
                        }
                    })
                    .then(function () {
                        cb(undefined);
                    }).catch(function (e) {
                        console.trace(e)
                    });
            } else {
                table = self.prefix + bucket;
                self.db(table)
                    .whereIn('key', keys)
                    .del()
                    .then(function () {
                        cb(undefined);
                    }).catch(function (e) {
                        console.trace(e)
                    });
            }
        });
    },

    /**
    	Removes values from a given key inside a bucket.
    */
    remove: function (transaction, bucket, key, values) {
        contract(arguments)
            .params('array', 'string', 'string|number', 'string|array')
            .end();

        var self = this;
        var table = '';
        values = Array.isArray(values) ? values : [values]; // we always want to have an array for values

        transaction.push(function (cb) {

            if (bucket.indexOf('allows') != -1) {
                table = self.prefix + self.buckets.permissions;
                var query = self.db
                    .select('key', 'value')
                    .from(table)
                    .where({
                        'key': bucket
                    })


                query
                    .then(function (result) {
                        if (result.length === 0) {
                            return;
                        }
                        result[0].value = JSON.parse(result[0].value)

                        // update the permissions for the role by removing what was requested
                        _.each(values, function (value) {
                            result[0].value[key] = _.without(result[0].value[key], value);
                        });

                        //  if no more permissions in the role then remove the role
                        if (!result[0].value[key].length) {
                            result[0].value = _.omit(result[0].value, key);
                        }

                        return self.db(table)
                            .where({
                                'key': bucket
                            })
                            .update({
                                value: result[0].value
                            });
                    })
                    .then(function () {
                        cb(undefined);
                    }).catch(function (e) {
                        console.trace(e)
                    });
            } else {
                table = self.prefix + bucket;
                var query = self.db
                    .select('key', 'value')
                    .from(table)
                    .where({
                        'key': key
                    })
                query
                    .then(function (result) {
                        if (result.length === 0) {
                            return;
                        }

                        result[0].value = JSON.parse(result[0].value)
                        var resultValues = result[0].value;
                        // if we have found the key in the table then lets remove the values from it
                        _.each(values, function (value) {
                            resultValues = _.without(resultValues, value);
                        });
                        return self.db(table)
                            .where('key', key)
                            .update({
                                value: resultValues
                            });
                    })
                    .then(function () {
                        cb(undefined);
                    }).catch(function (e) {
                        console.trace(e)
                    });
            }
        });
    }
};

KnexDBBackend.prototype.setup = createTables;
KnexDBBackend.prototype.teardown = require('./databaseTasks').dropTables;

exports = module.exports = KnexDBBackend;