(function () {
    'use strict';

    di.service('database', database, 'settings')

    function database(settings) {
        var instances = {};

        return {
            conn: function (instanceId, databaseId) {

                instanceId = instanceId || settings.database.default;

                databaseId = databaseId || instanceId;

                if (typeof settings.database.connections[databaseId] == 'undefined') {
                    throw Error('Base de dados "' + databaseId + '" não reconhecida')
                }

                if (typeof instances[instanceId] == 'undefined' || instances[instanceId] === null) {
                    instances[instanceId] = require('knex')(settings.database.connections[databaseId]);
                }
                return instances[instanceId];
            }
        };
    }

})();