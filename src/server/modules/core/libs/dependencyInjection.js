(function () {
    'use strict';

    
    /**
     * Para maiores detalhes vide a documentação de BottleJs 
     * https://github.com/young-steveo/bottlejs
     */
    var bottle = new require('bottlejs')();
    
    module.exports = {
        /**
         * Retorna o componente com o nome especificado 
         */
        get: function get(name) {
            return bottle.container[name]
        },

        /**
         * Registra uma constante
         */
        constant: function constant() {
            bottle.constant.apply(bottle, arguments)
        },

        /**
         * Regisra um decorator
         */
        decorator: function decorator() {
            bottle.decorator.apply(bottle, arguments)
        },

        /**
         * Registra uma fábrica
         */
        factory: function factory() {
            bottle.factory.apply(bottle, arguments)
        },

        /**
         * Registra um serviço
         */
        service: function service() {
            bottle.service.apply(bottle, arguments)
        },

        /**
         * Registra uma instancia de fabrica
         */
        instanceFactory: function instanceFactory() {
            bottle.instanceFactory.apply(bottle, arguments)
        },

    }

})()