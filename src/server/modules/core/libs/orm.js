(function () {

    di.service('orm', orm, 'database')

    function orm(database) {

        var instances = {};

        return {
            conn: function (instanceId, databaseId) {

                var bd = database.conn(databaseId || instanceId);

                instanceId = instanceId || 'default';

                if (typeof instances[instanceId] == 'undefined' || instances[instanceId] === null) {
                    instances[instanceId] = require('bookshelf')(bd);
                }
                return instances[instanceId];
            }
        };

    }

})()