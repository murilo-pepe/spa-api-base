require('..//modules/bootstrap')
var srvPermissoes = di.get('aclService')

exports.seed = function (knex, Promise) {
    {
        return knex('usuarios')
            .insert([
                //Senha é sorvete123 (sha1 da senha + salt)
                {
                    id: 1,
                    nome: 'Administrador',
                    email: 'admin@maisresultado.com.br',
                    senha: '8f7b0bd7ba05ab352fd15ec576578674ce9d34c8',
                    salt: 764675462
                }, {
                    id: 2,
                    nome: 'Painel',
                    email: 'painel@maisresultado.com.br',
                    senha: '8f7b0bd7ba05ab352fd15ec576578674ce9d34c8',
                    salt: 764675462
                },
            ])
            .then(function () {
                return srvPermissoes
                    .permitir('administrador', '/demandas/faturamento-clientes', 'get')
            }).then(function () {
                return srvPermissoes
                    .atribuirPapel('admin@maisresultado.com.br', 'administrador')
            })
            .catch(function (err) {
                console.log(err);
            });
    }

}