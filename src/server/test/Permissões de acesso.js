var moment = require('moment')
var expect = require('chai').expect;

var srvPermissoes = di.get('aclService');

/**
 * ps.: Considere usuário como solicitante de um recurso, nao ncessáriamente usuario de sistema
 */
describe('Ao inserir permissões', function () {

    it('Deve armazenar a permissão de um usuário para um recurso', function (done) {
        srvPermissoes
            .permitir('perTeste', 'recurso', 'permissao')
            .then(function () {
                return srvPermissoes.atribuirPapel('usuario_solicitante', 'perTeste')
            })
            .then(function () {
                return srvPermissoes.getPermissoes('usuario_solicitante', 'recurso')
            })
            .then(function (permissoes) {
                expect(permissoes).to.have.property('recurso').that.contain('permissao')
                done()
            })
            .catch(function (e) {
                done(e)
            })

    });

    it('Deve retornar corretamente as permissões armazenadas para um usuário', function (done) {
        srvPermissoes
            .isPermitido('usuario_solicitante', 'recurso', 'permissao')
            .then(function (valor) {
                expect(valor).to.be.true
                done()
            })
            .catch(function (e) {
                done(e)
            })

    })



});