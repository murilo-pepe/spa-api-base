 var bd = di.get('database').conn();


 before('Migrações', function (done) {
     this.timeout(30 * 1000)

     bd.migrate.currentVersion().then(console.log)

     bd.migrate.latest({
             directory: __dirname + '/../migrations'
         })
         .then(function () {
             bd.seed.run({
                 directory: __dirname + '/../test/seeds'
             }).then(function () {
                 done()
             })
         })

 })