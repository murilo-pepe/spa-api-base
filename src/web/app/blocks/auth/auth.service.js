(function() {
    'use strict';

    angular
        .module('blocks.auth')
        .factory('authService', authService);

    authService.$inject = ['$http', '$cookies', 'api'];

    var usuarioLogado = false;

    function authService($http, $cookies, api) {
        var service = {};

        service.login = login;
        service.definirCredenciais = definirCredenciais;
        service.limparCredenciais = limparCredenciais;
        service.getUsuarioLogado = getUsuarioLogado;
        service.restaurarCredenciais = restaurarCredenciais;
        service.getUsuarioFromToken = getUsuarioFromToken;
        service.loginViaToken = loginViaToken;
        service.possuiPermissao = possuiPermissao

        function login(email, senha) {
            return $http
                .post(api.url + 'auth/login', {
                    email: email,
                    senha: senha
                })
                .then(function(result) {
                    definirCredenciais(result.data.token, result.data.usuario);
                    return result.data;
                })
                .catch(function(parameters) {
                    return Promise.reject(parameters.data);
                });
        }

        function loginViaToken(token) {
            return service
                .getUsuarioFromToken(token)
                .then(function(usuario) {
                    service.definirCredenciais(token, usuario);
                    return true;
                })
                .catch(function(parameters) {
                    return Promise.reject(parameters.data);
                });
        }

        function getUsuarioLogado() {
            if (usuarioLogado === false) {
                restaurarCredenciais();
            }
            return usuarioLogado;
        }

        function getUsuarioFromToken(token) {
            return $http
                .get(api.url + 'auth/token/' + token + '/user')
                .then(function(result) {
                    return result.data;
                })
                .catch(function(parameters) {
                    return Promise.reject(parameters);
                });
        }

        function possuiPermissao(usuario, recurso, permissao) {
            return new Promise(function(resolve, reject) {
                return $http
                    .get(api.url + 'auth/is_allowed/' + encodeURIComponent(recurso) + '/' + encodeURIComponent(permissao))
                    .then(function(result) {
                        if (!result.data) {
                            throw new AuthorizationError();
                        }

                        return resolve(result.data);
                    })
                    .catch(function(parameters) {
                        return reject(parameters);
                    });
            });
        }

        function definirCredenciais(token, usuario) {
            usuarioLogado = usuario;

            $http.defaults.headers.common['x-token'] = token;
            $cookies.putObject('usuario', usuario);
            $cookies.put('token', token);
        }

        function restaurarCredenciais() {
            var usuarioAux = $cookies.getObject('usuario');
            if (typeof usuarioAux !== 'undefined') {
                usuarioLogado = usuarioAux;
                $http.defaults.headers.common['x-token'] = $cookies.get('token');
                return usuarioLogado;
            } else {
                return false;
            }
        }

        function limparCredenciais() {
            usuarioLogado = false;
            $cookies.remove('usuario');
            $cookies.remove('token');
            $http.defaults.headers.common['x-token'] = null;
        }

        return service;
    }


    function AuthorizationError() {
        this.message = 'Permissão negada'
        this.description = 'Vocês não possui permissão para acessar esse recurso'
    }
    AuthorizationError.prototype = Object.create(Error.prototype);
    AuthorizationError.prototype.constructor = AuthorizationError
})();