(function () {
    'use strict';

    angular
            .module('blocks.exception')
            .provider('exceptionHandler', exceptionHandlerProvider)
            .config(config);

    config.$inject = ['$provide'];


    function exceptionHandlerProvider() {
        this.config = {
            appErrorPrefix: undefined
        };

        this.configure = function (appErrorPrefix) {
            this.config.appErrorPrefix = appErrorPrefix;
        };

        this.$get = function () {
            return {config: this.config};
        };
    }

    function config($provide) {
        $provide.decorator('$exceptionHandler', extendExceptionHandler);
    }

    /**
     * Extend the $exceptionHandler service to also display a toast.
     * @param  {Object} $delegate
     * @param  {Object} exceptionHandler
     * @param  {Object} logger
     * @return {Function} the decorated $exceptionHandler service
     */
    extendExceptionHandler.$inject = ['$delegate', 'exceptionHandler', 'logger'];
    function extendExceptionHandler($delegate, exceptionHandler, logger) {
        return function (exception, cause) {
            var appErrorPrefix = exceptionHandler.config.appErrorPrefix || '';
            var errorData = {exception: exception, cause: cause};

            if (typeof exception.message !== 'undefined') {
                exception.message = appErrorPrefix + exception.message;
            } else {
                exception = {message: appErrorPrefix + exception};
            }

            $delegate(exception, cause);

            logger.error(exception.message, errorData);


        };
    }
})();
