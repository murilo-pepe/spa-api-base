(function() {
    'use strict';

    angular.module('app.cockpit', ['gridster', 'smart-table', 'ui.bootstrap']);

})();