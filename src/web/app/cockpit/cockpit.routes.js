(function () {
    'use strict';
    angular
        .module('app.cockpit')
        .run(appRun);

    appRun.$inject = ['routehelper']

    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [{
                url: '/',
                config: {
                    templateUrl: 'app/cockpit/views/status-bd.html',
                    controller: 'cockpitStatusBancoController',
                    controllerAs: 'vm',
                    title: 'Monitoramento das bases de dados',
                    requireAuth: true
                }
            },
            {
                url: '/status-bd',
                config: {
                    templateUrl: 'app/cockpit/views/status-bd.html',
                    controller: 'cockpitStatusBancoController',
                    controllerAs: 'vm',
                    title: 'Monitoramento das bases de dados',
                    requireAuth: true
                }
            },
            {
                url: '/lancamento-horas',
                config: {
                    templateUrl: 'app/cockpit/views/lancamento-horas.html',
                    controller: 'cockpitLancamentoHorasController',
                    controllerAs: 'vm',
                    title: 'Controle de horas',
                    requireAuth: true
                }
            },
            {
                url: '/indice-horas-projetos',
                config: {
                    templateUrl: 'app/cockpit/views/indice-horas-projetos.html',
                    controller: 'cockpitIndiceHorasProjetosController',
                    controllerAs: 'vm',
                    title: 'Horas estimadas x realizadas',
                    requireAuth: true
                }
            },
            {
                url: '/indicadores-sms',
                config: {
                    templateUrl: 'app/cockpit/views/indicadores-sms.html',
                    controller: 'cockpitIndicadoresSmsController',
                    controllerAs: 'vm',
                    title: 'Indicadores SMS',
                    requireAuth: true
                }
            },
            {
                url: '/indicadores-email',
                config: {
                    templateUrl: 'app/cockpit/views/indicadores-email.html',
                    controller: 'cockpitIndicadoresEmailController',
                    controllerAs: 'vm',
                    title: 'Indicadores Email',
                    requireAuth: true
                }
            },
            {
                url: '/indicadores-voz',
                config: {
                    templateUrl: 'app/cockpit/views/indicadores-voz.html',
                    controller: 'cockpitIndicadoresVozController',
                    controllerAs: 'vm',
                    title: 'Indicadores Voz',
                    requireAuth: true
                }
            },
            {
                url: '/faturamento-clientes',
                config: {
                    templateUrl: 'app/cockpit/views/faturamento-clientes.html',
                    controller: 'cockpitFaturamentoClientesController',
                    controllerAs: 'vm',
                    title: 'Faturamento por clientes',
                    requireAuth: true,
                    requirePermission: ['/demandas/faturamento-clientes', 'get']
                }
            }, 
            {
                url: '/fila-demandas',
                config: {
                    templateUrl: 'app/cockpit/views/grid-dinamico.html',
                    controller: 'cockpitGridDinamicoController',
                    controllerAs: 'vm',
                    title: 'Filas de demandas',
                    requireAuth: true
                }
            }, 
            {
                url: '/financeiro-gsm',
                config: {
                    templateUrl: 'app/cockpit/views/grid-dinamico.html',
                    controller: 'cockpitFinanceiroGsmController',
                    controllerAs: 'vm',
                    title: 'Faturamento GSM',
                    requireAuth: true
                }
            }
        ];
    }
})();