(function() {
    'use strict';
    angular
        .module('app.cockpit')
        .run(appRun);

    appRun.$inject = ['$location', '$rootScope', 'logger']

    function appRun($location, $rootScope, logger) {
        $rootScope.rotacionarRotas = false
        $rootScope.toggleRotacionarRotas = function() {
            $rootScope.rotacionarRotas = !$rootScope.rotacionarRotas
            proximaRota();
            logger.info('Modo apresentação ' + ($rootScope.rotacionarRotas ? 'ligado' : 'desligado'))
        }

        $rootScope.logout = function() {
            $rootScope.blur = false
            $location.path('/logout')
        }

        var arrRotas = [
            '/status-bd',
            '/indicadores-sms',
            '/indicadores-email',
            '/indicadores-voz',
            '/lancamento-horas',
            '/indice-horas-projetos',
            '/fila-demandas',
            
        ];
        var i = 0;


        setInterval(function() {
            proximaRota();

        }, 1000 * 60)

        function proximaRota() {
            if ($rootScope.rotacionarRotas) {
                $location.path(arrRotas[i]);
                i++;

                if (i >= arrRotas.length) {
                    i = 0;
                }
            }
        }

    }


})();