(function () {
    'use strict';

    angular
        .module('app.cockpit')
        .controller('cockpitFaturamentoClientesController', cockpitFaturamentoClientesController);

    cockpitFaturamentoClientesController.$inject = ['indiceHorasProjetosService', 'logger', '$scope'];

    function cockpitFaturamentoClientesController(indiceHorasProjetosService, logger, $scope) {
        var vm = this;

        vm.faturamentos = []
        vm.dataI = moment().startOf('year').toDate()
        vm.dataF = moment().endOf('day').toDate()
        vm.dataIOptions = {
            maxDate: vm.dataF,

        }
        vm.dataFOptions = {
            minDate: vm.dataF,
        }

        vm.datepickerAberto = false

        vm.carregando = false

        activate();

        ////////////////

        $scope.$watch('vm.dataI', function () {
            vm.dataFOptions.minDate = vm.dataI
            coletarValores()
        })

        $scope.$watch('vm.dataF', function () {
            vm.dataIOptions.maxDate = vm.dataF
            coletarValores()
        })

        function activate() {
            coletarValores();
        }

        function coletarValores() {
            vm.carregando = true
            vm.faturamentos = []
            indiceHorasProjetosService
                .getFaturamentoClientes(vm.dataI, vm.dataF)
                .then(function (faturamentos) {

                    for (var i = faturamentos.length - 1; i >= 0; i--) {

                        faturamentos[i].indice_faturamento =
                            faturamentos[i].faturamento_total > 0 ?
                            _.round(faturamentos[i].demandas.valor / faturamentos[i].faturamento_total, 2) :
                            Infinity;

                        if ((faturamentos[i].faturamento_total == 0) && (faturamentos[i].demandas.valor == 0)) {
                            faturamentos.splice(i, 1)
                        }

                    }

                    vm.faturamentos = faturamentos
                    vm.carregando = false
                })
                .catch(function (err) {

                    logger.error(err.message);
                });
        }
    }
})();