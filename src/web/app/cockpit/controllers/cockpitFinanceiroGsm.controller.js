(function() {
    'use strict';


    angular
        .module('app.cockpit')
        .controller("cockpitFinanceiroGsmController", cockpitFinanceiroGsmController);

    cockpitFinanceiroGsmController.$inject = ["databaseStatusService", "$scope", "logger"];

    function cockpitFinanceiroGsmController(databaseStatusService, $scope, logger) {
        var vm = this;
        var nextWidgetId = 1;

        vm.gridsterOpts = {
            columns: 24, // the width of the grid, in columns
            pushing: true, // whether to push other items out of the way on move or resize
            floating: false, // whether to automatically float items up so they stack (you can temporarily disable if you are adding unsorted items with ng-repeat)
            swapping: true, // whether or not to have items of the same size switch places instead of pushing down if they are the same size
            width: 'auto', // can be an integer or 'auto'. 'auto' scales gridster to be the full width of its containing element
            colWidth: 'auto', // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
            rowHeight: 'match', // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
            margins: [25, 25], // the pixel distance between each widget
            outerMargin: true, // whether margins apply to outer edges of the grid
            sparse: false, // "true" can increase performance of dragging and resizing for big grid (e.g. 20x50)
            isMobile: false, // stacks the grid items if true
            mobileBreakPoint: 600, // if the screen is not wider that this, remove the grid layout and stack the items
            mobileModeEnabled: true, // whether or not to toggle mobile mode when screen width is less than mobileBreakPoint
            minColumns: 1, // the minimum columns the grid must have
            minRows: 1, // the minimum height of the grid, in rows
            maxRows: 100,
            defaultSizeX: 2, // the default width of a gridster item, if not specifed
            defaultSizeY: 2, // the default height of a gridster item, if not specified
            minSizeX: 1, // minimum column width of an item
            maxSizeX: null, // maximum column width of an item
            minSizeY: 1, // minumum row height of an item
            maxSizeY: null, // maximum row height of an item
            resizable: {
                enabled: true,
                handles: ['se'],
                start: function(event, $element, widget) {}, // optional callback fired when resize is started,
                resize: function(event, $element, widget) {
                    $(window).trigger('resize');
                }, // optional callback fired when item is resized,
                stop: function(event, $element, widget) {
                    $(window).trigger('resize');
                } // optional callback fired when item is finished resizing
            },
            draggable: {
                enabled: true, // whether dragging items is supported
                handle: null, // optional selector for drag handle
                start: function(event, $element, widget) {}, // optional callback fired when drag is started,
                drag: function(event, $element, widget) {}, // optional callback fired when item is moved,
                stop: function(event, $element, widget) {} // optional callback fired when item is finished dragging
            }
        };

        vm.widgets = [];


        function addWidget(widget) {
            widget.id = nextWidgetId;
            nextWidgetId++;
            vm.widgets.push(widget);
        }

        function activate() {
            //Atendimento
            addWidget({
                controller: 'ragTimelineReportWidgetController',
                template: 'app/cockpit/widgets/ragTimelineReport/ragTimelineReportWidget.view.html',
                size: [22, 5],
                position: [1, 0],
                options: {
                    titulo: "Status geral - GSM",
                }
            })

        }

        activate();
    }

})();