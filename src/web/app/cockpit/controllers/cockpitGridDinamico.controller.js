(function () {
    'use strict';
    angular
        .module('app.cockpit')
        .controller("cockpitGridDinamicoController", cockpitGridDinamicoController);

    cockpitGridDinamicoController.$inject = ["databaseStatusService", "$scope", "logger"];

    function cockpitGridDinamicoController(databaseStatusService, $scope, logger) {
        var vm = this;
        var nextWidgetId = 1;

        vm.gridsterOpts = {
            columns: 24, // the width of the grid, in columns
            pushing: true, // whether to push other items out of the way on move or resize
            floating: false, // whether to automatically float items up so they stack (you can temporarily disable if you are adding unsorted items with ng-repeat)
            swapping: true, // whether or not to have items of the same size switch places instead of pushing down if they are the same size
            width: 'auto', // can be an integer or 'auto'. 'auto' scales gridster to be the full width of its containing element
            colWidth: 'auto', // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
            rowHeight: 'match', // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
            margins: [25, 25], // the pixel distance between each widget
            outerMargin: true, // whether margins apply to outer edges of the grid
            sparse: false, // "true" can increase performance of dragging and resizing for big grid (e.g. 20x50)
            isMobile: false, // stacks the grid items if true
            mobileBreakPoint: 600, // if the screen is not wider that this, remove the grid layout and stack the items
            mobileModeEnabled: true, // whether or not to toggle mobile mode when screen width is less than mobileBreakPoint
            minColumns: 1, // the minimum columns the grid must have
            minRows: 1, // the minimum height of the grid, in rows
            maxRows: 100,
            defaultSizeX: 2, // the default width of a gridster item, if not specifed
            defaultSizeY: 2, // the default height of a gridster item, if not specified
            minSizeX: 1, // minimum column width of an item
            maxSizeX: null, // maximum column width of an item
            minSizeY: 1, // minumum row height of an item
            maxSizeY: null, // maximum row height of an item
            resizable: {
                enabled: true,
                handles: ['se'],
                start: function (event, $element, widget) {}, // optional callback fired when resize is started,
                resize: function (event, $element, widget) {
                    $(window).trigger('resize');
                }, // optional callback fired when item is resized,
                stop: function (event, $element, widget) {
                    $(window).trigger('resize');
                } // optional callback fired when item is finished resizing
            },
            draggable: {
                enabled: true, // whether dragging items is supported
                handle: null, // optional selector for drag handle
                start: function (event, $element, widget) {}, // optional callback fired when drag is started,
                drag: function (event, $element, widget) {}, // optional callback fired when item is moved,
                stop: function (event, $element, widget) {} // optional callback fired when item is finished dragging
            }
        };

        vm.widgets = [];


        function addWidget(widget) {
            widget.id = nextWidgetId;
            nextWidgetId++;
            vm.widgets.push(widget);
        }

        function activate() {

            //Atendimento

           addWidget({
                controller: 'filaDemandasJiraWidgetController',
                template: 'app/cockpit/widgets/fila-demandas-jira/filaDemandasJiraWidget.view.html',
                size: [12, 6],
                options: {
                    titulo: "Atendimento - Fila de demandas",
                    projeto: 'atendimento',
                    status: ['Waiting for support', 'Awaiting approval', 'In Progress']
                }
            })


            addWidget({
                controller: 'vazaoStatusJiraWidgetController',
                template: 'app/cockpit/widgets/vazao-status-jira/vazaoStatusJiraWidget.view.html',
                size: [3, 3],
                options: {
                    titulo: "Atend: Abertos hoje",
                    projeto: 'atendimento',
                }
            })

            addWidget({
                controller: 'indicadorStatusJiraWidgetController',
                template: 'app/cockpit/widgets/indicador-status-jira/indicadorStatusJiraWidget.view.html',
                size: [3, 3],
                options: {
                    titulo: "Atend: Aguardando cliente",
                    projeto: 'atendimento',
                    status: ["Aguardando Cliente", 'Waiting for customer']
                }
            })

            addWidget({
                controller: 'indicadorStatusJiraWidgetController',
                template: 'app/cockpit/widgets/indicador-status-jira/indicadorStatusJiraWidget.view.html',
                size: [3, 3],
                options: {
                    titulo: "Atend: Aguardando OnGoing",
                    projeto: 'atendimento',
                    status: ["Aguardando Ongoing"]
                }
            })

            addWidget({
                controller: 'indicadorAtrasoJiraWidgetController',
                template: 'app/cockpit/widgets/indicador-atraso-jira/indicadorAtrasoJiraWidget.view.html',
                size: [3, 3],
                options: {
                    titulo: "Atend: Tempo médio",
                    tipo: 'avg',
                    projeto: 'atendimento',
                    status: ['Waiting for support', 'Awaiting approval', 'In Progress']
                }
            })


            addWidget({
                controller: 'vazaoStatusJiraWidgetController',
                template: 'app/cockpit/widgets/vazao-status-jira/vazaoStatusJiraWidget.view.html',
                size: [4, 3],
                options: {
                    titulo: "Atend: Encerrados hoje",
                    projeto: 'atendimento',
                    status: ["Resolvido"]
                }
            })


            addWidget({
                controller: 'indicadorStatusJiraWidgetController',
                template: 'app/cockpit/widgets/indicador-status-jira/indicadorStatusJiraWidget.view.html',
                size: [4, 3],
                options: {
                    titulo: "Atend: Aguardando aprovação",
                    projeto: 'atendimento',
                    status: ["Awaiting approval"]
                }
            })

            addWidget({
                controller: 'indicadorAtrasoJiraWidgetController',
                template: 'app/cockpit/widgets/indicador-atraso-jira/indicadorAtrasoJiraWidget.view.html',
                size: [4, 3],
                options: {
                    titulo: "Atend: Tempo máximo",
                    tipo: 'max',
                    projeto: 'atendimento',
                    status: ['Waiting for support', 'Awaiting approval', 'In Progress']
                }
            })



            //OnGoing
            addWidget({
                controller: 'filaDemandasJiraWidgetController',
                template: 'app/cockpit/widgets/fila-demandas-jira/filaDemandasJiraWidget.view.html',
                size: [12, 6],
                position: [0, 6],
                options: {
                    titulo: "Ongoing: Fila de demandas",
                    projeto: 'ongoing',
                    status: ["To Do", "In Review", "Aguardando Classificação", "In Progress"]
                }
            })


            addWidget({
                controller: 'vazaoStatusJiraWidgetController',
                template: 'app/cockpit/widgets/vazao-status-jira/vazaoStatusJiraWidget.view.html',
                size: [4, 3],
                options: {
                    titulo: "Ongoing: Abertos hoje",
                    projeto: 'ongoing'
                }
            })

            addWidget({
                controller: 'indicadorStatusJiraWidgetController',
                template: 'app/cockpit/widgets/indicador-status-jira/indicadorStatusJiraWidget.view.html',
                size: [4, 3],
                options: {
                    titulo: "Ongoing: Aguardando cliente/suporte",
                    projeto: 'ongoing',
                    status: ["Aguardando Cliente"]
                }
            })

            addWidget({
                controller: 'indicadorAtrasoJiraWidgetController',
                template: 'app/cockpit/widgets/indicador-atraso-jira/indicadorAtrasoJiraWidget.view.html',
                size: [4, 3],
                options: {
                    titulo: "Ongoing: Tempo médio",
                    tipo: 'avg',
                    projeto: 'ongoing',
                    status: ["To Do", "In Review", "Aguardando Classificação", "In Progress"]
                }
            })

            addWidget({
                controller: 'vazaoStatusJiraWidgetController',
                template: 'app/cockpit/widgets/vazao-status-jira/vazaoStatusJiraWidget.view.html',
                size: [4, 3],
                options: {
                    titulo: "Ongoing: Encerrados hoje",
                    projeto: 'ongoing',
                    status: ["Done"]
                }
            })
            addWidget({
                controller: 'indicadorStatusJiraWidgetController',
                template: 'app/cockpit/widgets/indicador-status-jira/indicadorStatusJiraWidget.view.html',
                size: [4, 3],
                options: {
                    titulo: "Ongoing: Aguardando classificação",
                    projeto: 'ongoing',
                    status: ["Aguardando Classificação"]
                }
            })



            addWidget({
                controller: 'indicadorAtrasoJiraWidgetController',
                template: 'app/cockpit/widgets/indicador-atraso-jira/indicadorAtrasoJiraWidget.view.html',
                size: [4, 3],
                options: {
                    titulo: "Ongoing: Tempo máximo",
                    tipo: 'max',
                    projeto: 'ongoing',
                    status: ["To Do", "In Review", "Aguardando Classificação", "In Progress"]
                }
            })




        }

        activate();

    }

    ////////////////////////////

    angular
        .module('app.cockpit')
        .directive('widgetDirective', fooDirective)

    function fooDirective() {
        return {
            scope: {
                widget: '=',
            },
            template: '<ng-include src="getTemplateUrl()"/>',
            controller: ['$controller', '$scope', function ($controller, $scope) {
                $scope.getTemplateUrl = function () {
                    return $scope.widget.template
                }

                var foo = $controller($scope.widget.controller, {
                    $scope: $scope
                });

                return foo;
            }],
            controllerAs: 'vm',
            link: function ($scope, $element, $attrs, $ctrl) {}
        };
    }



    ////////////////////////////



    angular
        .module('app.cockpit')
        .directive('bigText', function () {
            return {
                restrict: 'A',
                link: function (scope, element, attr) {
                    var bt = element.bigtext({
                        maxfontsize: attr.maxSize || 90,
                        minfontsize: attr.minSize,
                        childSelector: attr.childSelector,
                        resize: true,
                    });
                    $(window).trigger('resize')
                    return bt
                }
            };
        });
})();