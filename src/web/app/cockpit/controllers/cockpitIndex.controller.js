(function () {
    'use strict';
    angular
        .module('app.cockpit')
        .controller("cockpitIndexController", cockpitIndexController);

    cockpitIndexController.$inject = ['$location'];

    function cockpitIndexController($location) {

        //////////////

        var vm = this; 
        vm.goTo = function (location) {
            $location.path(location);
        }


        //////////////////
        activate();

        function activate() {

        }



    }

})();
