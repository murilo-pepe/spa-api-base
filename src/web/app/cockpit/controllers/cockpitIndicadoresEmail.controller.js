(function() {
    'use strict';

    angular
        .module('app.cockpit')
        .controller('cockpitIndicadoresEmailController', cockpitIndicadoresEmailController);

    cockpitIndicadoresEmailController.$inject = ['indicadoresEmailService', '$scope', 'logger'];

    function cockpitIndicadoresEmailController(indicadoresEmailService, $scope, logger) {
        var vm = this;

        //Enviado/Previsto mes campanhas
        vm.qtdeCampanhaEnviadosMes = 0
        vm.diffMesEnviadosCampanha = 0
        vm.qtdePrevistoCampanhaMes = 0
        vm.diffMesPrevistosCampanha = 0

        vm.qtdeCampanhaEnviadoMesAnterior = 0
        vm.diffCampanhaEnviadoMesAnterior = 0

        vm.qtdeAvulsoEnviadoMesAnterior = 0
        vm.diffAvulsoEnviadoMesAnterior = 0

        vm.qtdeCampanhaPrevistoMesAnterior = 0
        vm.diffCampanhaPrevistoMesAnterior = 0

        //Enviado mes avulsos
        vm.qtdeAvulsoMes = 0
        vm.diffAvulsoMes = 0

        //Enviado/Previsto dia campanhas
        vm.qtdeDiaCampanhas = 0
        vm.qtdePrevistoCampanhasDia = 0

        //Enviado dia avulso
        vm.qtdeAvulsoDia = 0

        //Atraso de envio campanha
        vm.emailsAtrasadosCampanha = 0
        vm.campahasAtrasadas = 0
        vm.atrasoMedioCampanha = 0
        vm.atrasoMaximoCampanha = 0


        //Atraso de envio campanha
        vm.emailsAtrasadosAvulso = 0
        vm.atrasoMedioAvulso = 0
        vm.atrasoMaximoAvulso = 0

        //Vazao
        vm.vazaoCampanha = 0
        vm.vazaoAvulso = 0


        //////////////////////////////
        var amostraVazao = 60

        var calcVazao = [{
            enviadosCampanha: 0,
            enviadosAvulso: 0,
            data: new moment()
        }]

        var grafSlaDiaCampanha = null
        var grafSlaSemanaCampanha = null
        var grafSlaMesCampanha = null

        var grafSlaDiaAvulso = null
        var grafSlaSemanaAvulso = null
        var grafSlaMesAvulso = null

        var grafDistCampanhaMes = null
        var grafDistCampanhaDia = null

        var grafDistAvulsoMes = null
        var grafDistAvulsoDia = null

        var grafFilaEnvio = null

        var pieOptions = {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                type: 'pie'
            },
            exporting: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            title: {
                align: 'center',
                verticalAlign: 'middle',
                y: 45
            },
            tooltip: {
                headerFormat: '{point.key}',
                pointFormat: ': {point.y} ({point.percentage:.1f}%)'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',


                    dataLabels: {
                        enabled: true,
                        distance: -20,
                        //format: '{point.percentage:.0f} %',
                        formatter: function() {
                            if (this.point.percentage.toFixed(1) > 0)
                                return this.point.percentage.toFixed(1) + '%'
                        },
                        style: {
                            color: "#FFFFFF",
                            fontSize: "14px",
                            fontWeight: "normal",
                            textOutline: "1px 0px #333333"
                        }
                    },
                    colors: ['#55BF3B', '#FF731C', '#DD0000'],
                    size: '160',
                    startAngle: -90,
                    endAngle: 90,
                    center: ['50%', '60'],
                    showInLegend: false,
                    innerSize: '50%',
                    colorByPoint: true,
                    name: 'Status',
                }
            },

        }

        ///////////////
        function gerarGraficoFila() {
            grafFilaEnvio = Highcharts.chart('graf-fila-envio', {
                chart: {
                    type: 'area'
                },
                legend: {
                    itemStyle: {
                        fontSize: '20px'
                    }
                },
                exporting: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                xAxis: {
                    type: 'datetime',
                },
                yAxis: {
                    title: {
                        text: 'Na fila'
                    }
                },
                plotOptions: {
                    area: {
                        stacking: 'normal',
                        connectNulls: true // this will stop the "serrated" appearance
                    }
                },
                series: [{
                    name: 'Campanhas',
                    data: []
                }, {
                    name: 'Avulso',
                    data: []
                }, ]
            });
        }

        function gerarGraficoSla(idContainer, titulo) {
            return Highcharts
                .chart(idContainer, Highcharts.merge(pieOptions, {
                    title: {
                        text: titulo
                    },
                    series: [{
                        data: [{
                            name: 'Ok',
                            y: 0
                        }, {
                            name: 'Alerta',
                            y: 0
                        }, {
                            name: 'Atraso',
                            y: 0
                        }],
                    }]
                }))
        }

        function gerarGraficoDistribuicao(idContainer) {
            return Highcharts
                .chart(idContainer, Highcharts.merge({
                    chart: {
                        type: 'bar',
                        backgroundColor: null,
                        borderWidth: 0,
                        margin: [0, 0, 35, 0],
                        padding: 0
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    legend: {
                        margin: 0,
                        floating: true,
                        verticalAlign: 'bottom',
                        y: 15,
                        itemDistance: 0,
                        itemStyle: {
                            fontSize: '10px'
                        }
                    },
                    tooltip: {
                        headerFormat: '',
                        pointFormat: '{series.name}: <b>{point.y} ({point.percentage:.1f}%)</b>'
                    },
                    yAxis: {
                        endOnTick: false,
                        startOnTick: false,
                        labels: {
                            enabled: false
                        },
                        title: {
                            text: null
                        },
                        tickPositions: [0]

                    },
                    xAxis: {
                        labels: {
                            enabled: false
                        },
                        title: {
                            text: null
                        },
                        startOnTick: false,
                        endOnTick: false,
                        tickPositions: []
                    },
                    plotOptions: {
                        bar: {
                            stacking: 'percent',
                        },

                        series: {
                            dataLabels: {
                                enabled: true,
                                align: 'center',
                                color: '#FFFFFF',
                                //format: '{point.percentage:.0f}%',

                                formatter: function() {
                                    if (this.point.percentage.toFixed(1) > 0)
                                        return this.point.percentage.toFixed(1) + '%'
                                },
                                style: {
                                    color: "#FFFFFF",
                                    fontSize: "12px",
                                    fontWeight: "normal",
                                    textOutline: "1px 0px #333333"
                                }
                            },
                        }

                    },
                    series: [{
                        name: 'Spam',
                        data: [0],
                    }, {
                        name: 'Cancelado',
                        data: [0],
                    }, {
                        name: 'Descadastrado',
                        data: [0],
                    }, {
                        name: 'Inexistente',
                        data: [0],
                    }, {
                        name: 'Não entregue',
                        data: [0],
                    }, {
                        name: 'Clicado',
                        data: [0],
                    }, {
                        name: 'Aberto',
                        data: [0],
                        color: '#5555AA'
                    }, {
                        name: 'Entregue',
                        data: [0],
                        color: '#44AA44'
                    }]
                }))
        }

        activate();

        ////////////////

        function activate() {

            setTimeout(function() {
                gerarGraficoFila()
                grafSlaDiaCampanha = gerarGraficoSla('graf-entregue-dia-campanha', 'Dia')
                grafSlaSemanaCampanha = gerarGraficoSla('graf-entregue-semana-campanha', 'Semana')
                grafSlaMesCampanha = gerarGraficoSla('graf-entregue-mes-campanha', 'Mês')

                grafSlaDiaAvulso = gerarGraficoSla('graf-entregue-dia-avulso', 'Dia')
                grafSlaSemanaAvulso = gerarGraficoSla('graf-entregue-semana-avulso', 'Semana')
                grafSlaMesAvulso = gerarGraficoSla('graf-entregue-mes-avulso', 'Mês')

                grafDistCampanhaMes = gerarGraficoDistribuicao('graf-campanha-mes')
                grafDistCampanhaDia = gerarGraficoDistribuicao('graf-campanha-dia')

                grafDistAvulsoMes = gerarGraficoDistribuicao('graf-avulso-mes')
                grafDistAvulsoDia = gerarGraficoDistribuicao('graf-avulso-dia')
                console.log(grafDistAvulsoDia)

                atualizarGraficos();
                var intervalo = setInterval(atualizarGraficos, 60 * 100);

                $scope.$on("$destroy", function() {
                    clearInterval(intervalo);
                });
            }, 1000);

        }


        function atualizarGraficos() {
            atualizarValoresDia();
            atualizarValoresSemana();
            atualizarValoresMesCorrente();
            atualizarValoresMesAnterior();
            atualizarValoresInstantaneo();
        }

        function atualizarValoresInstantaneo() {
            var inicioPeriodo = moment().startOf('day').toDate();
            var fimPeriodo = moment().endOf('day').toDate();

            var dataCaptura = Date.now();

            indicadoresEmailService
                .getFila(inicioPeriodo, fimPeriodo)
                .then(function(resumo) {

                    if (typeof resumo.campanha == 'undefined') {
                        return
                    }

                    vm.atrasoMaximoCampanha = resumo.campanha.fila_atraso_maximo
                    vm.atrasoMedioCampanha = resumo.campanha.fila_atrasado > 0 ?
                        resumo.campanha.fila_atraso_total / resumo.campanha.fila_atrasado :
                        0

                    vm.emailsAtrasadosCampanha = resumo.campanha.fila_atrasado
                    vm.campahasAtrasadas = resumo.campanha.campanhas_atrasadas


                    vm.atrasoMaximoAvulso = resumo.avulso.fila_atraso_maximo
                    vm.atrasoMedioAvulso = resumo.avulso.fila_atrasado > 0 ?
                        resumo.avulso.fila_atraso_total / resumo.avulso.fila_atrasado :
                        0

                    vm.emailsAtrasadosAvulso = resumo.avulso.fila_atrasado

                    if (grafFilaEnvio.series[0].data.length > 360) {
                        grafFilaEnvio.series[0].removePoint(0);
                        grafFilaEnvio.series[1].removePoint(0);
                    }

                    grafFilaEnvio.series[0].addPoint([dataCaptura, parseFloat(resumo.campanha.fila)]);
                    grafFilaEnvio.series[1].addPoint([dataCaptura, parseFloat(resumo.avulso.fila)]);

                })
                .catch(function(err) {
                    logger.error(err.message);
                });

        }

        function setValoresGrafDistribuicao(grafico, valores) {



            grafico.series[0].points[0]
                .update(Math.floor(valores.spams));

            grafico.series[1].points[0]
                .update(Math.floor(valores.cancelados));

            grafico.series[2].points[0]
                .update(Math.floor(valores.descadastrados));

            grafico.series[3].points[0]
                .update(Math.floor(valores.inexistentes));

            grafico.series[4].points[0]
                .update(Math.floor(valores.nao_entregues));

            grafico.series[5].points[0]
                .update(Math.floor(valores.clicados));

            grafico.series[6].points[0]
                .update(Math.floor(valores.abertos - valores.clicados));

            grafico.series[7].points[0]
                .update(Math.floor(valores.entregues - valores.abertos - valores.clicados));

        }


        function atualizarValoresDia() {
            var inicioDia = moment().startOf('day').toDate();
            var fimDia = moment().endOf('day').toDate();

            var dataCaptura = Date.now();

            indicadoresEmailService
                .getEstimativa(inicioDia, fimDia).then(function(resumo) {
                    vm.qtdePrevistoCampanhasDia = resumo.campanha
                })
                .catch(function(err) {
                    logger.error(err.message);
                });



            indicadoresEmailService
                .getResumo(inicioDia, fimDia)
                .then(function(resumo) {

                    if (typeof resumo.campanha == 'undefined') {
                        return
                    }

                    vm.qtdeDiaCampanhas = resumo.campanha.enviados
                    vm.qtdeAvulsoDia = resumo.avulso.enviados

                    setValoresGrafDistribuicao(grafDistCampanhaDia, resumo.campanha)

                    setValoresGrafDistribuicao(grafDistAvulsoDia, resumo.avulso)


                    grafSlaDiaCampanha.series[0].points[0] //No prazo
                        .update(Math.floor(resumo.campanha.enviados - resumo.campanha.enviados_slaAlerta));
                    grafSlaDiaCampanha.series[0].points[1] //Atrasado alerta
                        .update(Math.floor(resumo.campanha.enviados_slaAlerta - resumo.campanha.enviados_slaCritico));
                    grafSlaDiaCampanha.series[0].points[2] //Atrasado critico
                        .update(Math.floor(resumo.campanha.enviados_slaCritico));


                    grafSlaDiaAvulso.series[0].points[0] //No prazo
                        .update(Math.floor(resumo.avulso.enviados - resumo.avulso.enviados_slaAlerta));
                    grafSlaDiaAvulso.series[0].points[1] //Atrasado alerta
                        .update(Math.floor(resumo.avulso.enviados_slaAlerta - resumo.avulso.enviados_slaCritico));
                    grafSlaDiaAvulso.series[0].points[2] //Atrasado critico
                        .update(Math.floor(resumo.avulso.enviados_slaCritico));


                    if (calcVazao.length > amostraVazao) {
                        calcVazao.shift();
                    }

                    calcVazao.push({
                        enviadosCampanha: resumo.campanha.enviados,
                        enviadosAvulso: resumo.avulso.enviados,
                        data: moment(dataCaptura)
                    })

                    var diffSeconds = 0
                    var diffEnviadosCampanha = 0
                    var diffEnviadosAvulso = 0

                    for (var i in calcVazao) {
                        if (i == 0) {
                            continue;
                        }
                        diffSeconds += calcVazao[i].data.diff(calcVazao[i - 1].data, 'seconds');
                        diffEnviadosCampanha += calcVazao[i].enviadosCampanha - calcVazao[i - 1].enviadosCampanha
                        diffEnviadosAvulso += calcVazao[i].enviadosAvulso - calcVazao[i - 1].enviadosAvulso
                    }

                    vm.vazaoCampanha = diffEnviadosCampanha / diffSeconds * 60 * 60
                    vm.vazaoAvulso = diffEnviadosAvulso / diffSeconds * 60 * 60
                })
                .catch(function(err) {
                    logger.error(err.message);
                });

        }


        function atualizarValoresSemana() {
            var inicioSemana = moment().startOf('week').toDate();
            var fimSemana = moment().endOf('day').toDate();

            var dataCaptura = Date.now();

            indicadoresEmailService
                .getResumo(inicioSemana, fimSemana)
                .then(function(resumo) {

                    if (typeof resumo.campanha == 'undefined') {
                        return
                    }

                    grafSlaSemanaCampanha.series[0].points[0] //No prazo
                        .update(Math.floor(resumo.campanha.enviados - resumo.campanha.enviados_slaAlerta));
                    grafSlaSemanaCampanha.series[0].points[1] //Atrasado alerta
                        .update(Math.floor(resumo.campanha.enviados_slaAlerta - resumo.campanha.enviados_slaCritico));
                    grafSlaSemanaCampanha.series[0].points[2] //Atrasado critico
                        .update(Math.floor(resumo.campanha.enviados_slaCritico));

                    grafSlaSemanaAvulso.series[0].points[0] //No prazo
                        .update(Math.floor(resumo.avulso.enviados - resumo.avulso.enviados_slaAlerta));
                    grafSlaSemanaAvulso.series[0].points[1] //Atrasado alerta
                        .update(Math.floor(resumo.avulso.enviados_slaAlerta - resumo.avulso.enviados_slaCritico));
                    grafSlaSemanaAvulso.series[0].points[2] //Atrasado critico
                        .update(Math.floor(resumo.avulso.enviados_slaCritico));

                })
                .catch(function(err) {
                    logger.error(err.message);
                });

        }

        function atualizarValoresMesCorrente() {
            var inicioMes = moment().startOf('month').toDate();
            var fimMes = moment().endOf('day').toDate();

            var dataCaptura = Date.now();

            indicadoresEmailService
                .getEstimativa(inicioMes, fimMes).then(function(resumo) {
                    vm.qtdePrevistoCampanhaMes = resumo.campanha
                    vm.diffCampanhaPrevistoMesAnterior = vm.qtdePrevistoCampanhaMes > 0 ? (vm.qtdePrevistoCampanhaMes - vm.qtdeCampanhaEnviadoMesAnterior) / vm.qtdePrevistoCampanhaMes * 100 : 0

                })
                .catch(function(err) {
                    logger.error(err.message);
                });

            indicadoresEmailService
                .getResumo(inicioMes, fimMes)
                .then(function(resumo) {

                    if (typeof resumo.campanha == 'undefined') {
                        return
                    }

                    vm.qtdeCampanhaEnviadosMes = resumo.campanha.enviados
                    vm.qtdeAvulsoMes = resumo.avulso.enviados

                    vm.diffCampanhaEnviadoMesAnterior = (vm.qtdeCampanhaEnviadosMes - vm.qtdeCampanhaEnviadoMesAnterior) / vm.qtdeCampanhaEnviadosMes * 100;

                    vm.diffAvulsoEnviadoMesAnterior = (vm.qtdeAvulsoMes - vm.qtdeAvulsoEnviadoMesAnterior) / vm.qtdeAvulsoMes * 100;

                    setValoresGrafDistribuicao(grafDistCampanhaMes, resumo.campanha)
                    setValoresGrafDistribuicao(grafDistAvulsoMes, resumo.avulso)


                    grafSlaMesCampanha.series[0].points[0] //No prazo
                        .update(Math.floor(resumo.campanha.enviados - resumo.campanha.enviados_slaAlerta));
                    grafSlaMesCampanha.series[0].points[1] //Atrasado alerta
                        .update(Math.floor(resumo.campanha.enviados_slaAlerta - resumo.campanha.enviados_slaCritico));
                    grafSlaMesCampanha.series[0].points[2] //Atrasado critico
                        .update(Math.floor(resumo.campanha.enviados_slaCritico));


                    grafSlaMesAvulso.series[0].points[0] //No prazo
                        .update(Math.floor(resumo.avulso.enviados - resumo.avulso.enviados_slaAlerta));
                    grafSlaMesAvulso.series[0].points[1] //Atrasado alerta
                        .update(Math.floor(resumo.avulso.enviados_slaAlerta - resumo.avulso.enviados_slaCritico));
                    grafSlaMesAvulso.series[0].points[2] //Atrasado critico
                        .update(Math.floor(resumo.avulso.enviados_slaCritico));

                })
                .catch(function(err) {
                    logger.error(err.message);
                });

        }

        function atualizarValoresMesAnterior() {
            var inicioMesAnterior = moment().add(-1, 'month').startOf(1, 'month').toDate();
            var fimMesAnterior = moment().add(-1, 'month').startOf(1, 'month').toDate();

            indicadoresEmailService
                .getResumo(inicioMesAnterior, fimMesAnterior)
                .then(function(resumo) {
                    if (typeof resumo.campanha == 'undefined') {
                        return
                    }
                    vm.qtdeCampanhaEnviadoMesAnterior = resumo.campanha.enviados
                    vm.qtdeAvulsoEnviadoMesAnterior = resumo.avulso.enviados
                })
                .catch(function(err) {
                    logger.error(err.message);
                });


            indicadoresEmailService
                .getPrevisao(inicioMesAnterior, fimMesAnterior)
                .then(function(resumo) {
                    if (typeof resumo.campanha == 'undefined') {
                        return
                    }
                    vm.qtdeCampanhaPrevistoMesAnterior = resumo.campanha.previsto

                })
                .catch(function(err) {
                    logger.error(err.message);
                });

        }
    }
})();