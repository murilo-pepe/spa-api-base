(function() {
    'use strict';

    angular
        .module('app.cockpit')
        .controller('cockpitIndicadoresSmsController', cockpitIndicadoresSmsController);

    cockpitIndicadoresSmsController.$inject = ["indicadoresSmsService", '$scope', 'logger'];

    function cockpitIndicadoresSmsController(indicadoresSmsService, $scope, logger) {
        var vm = this;

        vm.grafFilaEnvio = null

        var gaugeOptions = {
            exporting: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            chart: {
                type: 'solidgauge'
            },
            title: null,
            pane: {
                center: ['50%', '90%'],
                size: '165%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: '#EEE',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },
            tooltip: {
                enabled: false
            },
            // the value axis
            yAxis: {
                min: 0,
                max: 100,
                title: {
                    // text: '% lançamento de horas'
                },
                stops: [
                    [0.74, '#DD0000'],
                    [0.84, '#FF731C'],
                    [0.94, '#DDDF0D'],
                    [1, '#55BF3B'],
                    [1.01, '#993399']
                ],
                lineWidth: 0,
                minorTickInterval: null,
                tickAmount: 2,
                title: {
                    y: -70
                },
                labels: false
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 15,
                        borderWidth: 0,
                        useHTML: true
                    }
                }
            }
        };

        vm.qtdeMesGSM = 0;
        vm.qtdeMesLA = 0;
        vm.qtdeDiaGSM = 0;
        vm.qtdeDiaLA = 0;
        vm.qtdeMesAnteriorGSM = 0;
        vm.qtdeMesAnteriorLA = 0;
        vm.diffMesGSM = 0;
        vm.diffMesLA = 0;

        vm.qtdePrevistoMesGSM = 0
        vm.qtdePrevistoMesLA = 0
        vm.qtdePrevistoDiaGSM = 0
        vm.qtdePrevistoDiaLA = 0
        vm.difPrevistoMesGSM = 0;
        vm.difPrevistoMesLA = 0;
        vm.diffPrevistoMesGSM = 0;
        vm.diffPrevistoMesLA = 0;


        vm.atrasoLA = 0;
        vm.atrasoGSM = 0;

        vm.maxAtrasoLA = 0;
        vm.maxAtrasoGSM = 0;

        vm.graficoEntregueDiaGSM = {};
        vm.graficoEntregueSemanaGSM = {};
        vm.graficoEntregueMesGSM = {};
        vm.graficoEntregueDiaLA = {};
        vm.graficoEntregueSemanaLA = {};
        vm.graficoEntregueMesLA = {};

        activate();

        ////////////////

        function activate() {
            setTimeout(function() {
                gerarGraficoFila()
                vm.graficoEntregueDiaGSM = gerarGrafico('graf-entregue-dia-gsm', 'Dia');
                vm.graficoEntregueSemanaGSM = gerarGrafico('graf-entregue-semana-gsm', 'Semana');
                vm.graficoEntregueMesGSM = gerarGrafico('graf-entregue-mes-gsm', 'Mês');
                vm.graficoEntregueDiaLA = gerarGrafico('graf-entregue-dia-la', 'Dia');
                vm.graficoEntregueSemanaLA = gerarGrafico('graf-entregue-semana-la', 'Semana');
                vm.graficoEntregueMesLA = gerarGrafico('graf-entregue-mes-la', 'Mês');

                atualizarGraficos();
                var intervalo = setInterval(atualizarGraficos, 60 * 100);

                $scope.$on("$destroy", function() {
                    clearInterval(intervalo);
                });
            }, 1000);
        }

        function gerarGrafico(idContainer, label) {
            return Highcharts
                .chart(idContainer, Highcharts.merge(gaugeOptions, {
                    series: [{
                        data: [0],
                        dataLabels: {
                            format: '<div style="text-align:center"><span style="font-size:32px;color:black">{y}</span><br/>' +
                                '<span style="font-size:18px;color:gray">% ' + label + '</span></div>'
                        },
                    }]
                }))
        }

        function atualizarGraficos() {
            atualizarValoresDia();
            atualizarValoresSemana();
            atualizarValoresMesCorrente();
            atualizarValoresMesAnterior();
            atualizarValoresInstantaneo();
        }

        function gerarGraficoFila() {
            vm.grafFilaEnvio = Highcharts.chart('graf-fila-envio', {
                chart: {
                    type: 'area'
                },
                legend: {
                    itemStyle: {
                        fontSize: '20px'
                    }
                },
                exporting: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                xAxis: {
                    type: 'datetime',
                },
                yAxis: {
                    title: {
                        text: 'Na fila'
                    }
                },
                plotOptions: {
                    area: {
                        stacking: 'normal',
                        connectNulls: true // this will stop the "serrated" appearance
                    }
                },
                series: [{
                    name: 'GSM',
                    data: []
                }, {
                    name: 'LA',
                    data: []
                }]
            });
        }

        function atualizarValoresInstantaneo() {
            var inicioPeriodo = moment().startOf('day').toDate();
            var fimPeriodo = moment().endOf('day').toDate();

            indicadoresSmsService
                .getResumoPeriodo(inicioPeriodo, fimPeriodo)
                .then(function(resumo) {

                    var dataCaptura = Date.now();

                    vm.atrasoGSM = resumo['GSM']['atraso_medio'];
                    vm.atrasoLA = resumo['LA']['atraso_medio'];

                    vm.maxAtrasoGSM = resumo['GSM']['atraso_maximo'];
                    vm.maxAtrasoLA = resumo['LA']['atraso_maximo'];

                    var i = 0;
                    for (var produto in resumo) {
                        if (typeof vm.grafFilaEnvio.series[i] == 'undefined') {
                            vm.grafFilaEnvio.addSeries({
                                name: resumo[produto]['produto'],
                                data: []
                            });
                        }

                        if (vm.grafFilaEnvio.series[i].data.length > 360) {
                            vm.grafFilaEnvio.series[i].removePoint(0);
                        }

                        vm.grafFilaEnvio.series[i].addPoint([dataCaptura, parseFloat(resumo[produto]['aguardando_envio'])]);
                        i++;
                    }


                })
                .catch(function(err) {
                    logger.error(err.message);
                });

        }

        function atualizarValoresDia() {
            var inicioDia = moment().startOf('day').toDate();
            var fimDia = moment().endOf('day').toDate();

            indicadoresSmsService
                .getResumoPeriodo(inicioDia, fimDia)
                .then(function(resumo) {
                    vm.qtdeDiaGSM = resumo.GSM.enviados
                    vm.qtdeDiaLA = resumo.LA.enviados

                    vm.qtdePrevistoDiaGSM = resumo.GSM.previsto
                    vm.qtdePrevistoDiaLA = resumo.LA.previsto


                    vm.atrasoGSM = resumo['GSM']['atraso_medio'];
                    vm.atrasoLA = resumo['LA']['atraso_medio'];

                    vm.maxAtrasoGSM = resumo['GSM']['atraso_maximo'];
                    vm.maxAtrasoLA = resumo['LA']['atraso_maximo'];

                    var porcentNoPrazoGSM = resumo.GSM.enviados > 0 ?
                        (resumo.GSM.enviados_no_prazo / resumo.GSM.enviados) * 100 :
                        0;
                    var porcentNoPrazoLA = resumo.LA.enviados > 0 ?
                        (resumo.LA.enviados_no_prazo / resumo.LA.enviados) * 100 :
                        0;


                    vm.graficoEntregueDiaGSM.series[0].points[0]
                        .update(Math.floor(porcentNoPrazoGSM));

                    vm.graficoEntregueDiaLA.series[0].points[0]
                        .update(Math.floor(porcentNoPrazoLA));

                })
                .catch(function(err) {
                    logger.error(err.message);
                });

        }


        function atualizarValoresSemana() {
            var inicioDia = moment().startOf('week').toDate();
            var fimDia = moment().endOf('week').toDate();

            indicadoresSmsService
                .getResumoPeriodo(inicioDia, fimDia)
                .then(function(resumo) {

                    var porcentNoPrazoGSM = resumo.GSM.enviados > 0 ? (resumo.GSM.enviados_no_prazo / resumo.GSM.enviados) * 100 : 0;
                    var porcentNoPrazoLA = resumo.LA.enviados > 0 ? (resumo.LA.enviados_no_prazo / resumo.LA.enviados) * 100 : 0;

                    vm.graficoEntregueSemanaGSM.series[0].points[0]
                        .update(Math.floor(porcentNoPrazoGSM));

                    vm.graficoEntregueSemanaLA.series[0].points[0]
                        .update(Math.floor(porcentNoPrazoLA));

                })
                .catch(function(err) {
                    logger.error(err.message);
                });

        }

        function atualizarValoresMesCorrente() {
            var inicioMes = moment().startOf('month').toDate();
            var fimMes = moment().endOf('month').toDate();

            indicadoresSmsService
                .getResumoPeriodo(inicioMes, fimMes)
                .then(function(resumo) {
                    vm.qtdeMesGSM = resumo.GSM.enviados
                    vm.qtdeMesLA = resumo.LA.enviados

                    vm.qtdePrevistoMesGSM = resumo.GSM.previsto
                    vm.qtdePrevistoMesLA = resumo.LA.previsto


                    var porcentNoPrazoGSM = resumo.GSM.enviados > 0 ? resumo.GSM.enviados_no_prazo / resumo.GSM.enviados * 100 : 0;
                    var porcentNoPrazoLA = resumo.LA.enviados > 0 ? resumo.LA.enviados_no_prazo / resumo.LA.enviados * 100 : 0;

                    vm.graficoEntregueMesGSM.series[0].points[0]
                        .update(Math.floor(porcentNoPrazoGSM));

                    vm.graficoEntregueMesLA.series[0].points[0]
                        .update(Math.floor(porcentNoPrazoLA));

                })
                .catch(function(err) {
                    logger.error(err.message);
                });

        }

        function atualizarValoresMesAnterior() {
            var inicioMesAnterior = moment().add(-1, 'month').subtract(1, 'month').toDate();
            var fimMesAnterior = moment().add(-1, 'month').subtract(1, 'month').toDate();

            indicadoresSmsService
                .getResumoPeriodo(inicioMesAnterior, fimMesAnterior)
                .then(function(resumo) {
                    vm.qtdeMesAnteriorGSM = resumo.GSM.enviados
                    vm.qtdeMesAnteriorLA = resumo.LA.enviados

                    vm.diffMesGSM = (vm.qtdeMesGSM - vm.qtdeMesAnteriorGSM) / vm.qtdeMesGSM * 100;
                    vm.diffMesLA = (vm.qtdeMesLA - vm.qtdeMesAnteriorLA) / vm.qtdeMesLA * 100;

                    vm.diffPrevistoMesGSM = (vm.qtdePrevistoMesGSM - resumo.GSM.previsto) / vm.qtdePrevistoMesGSM * 100;
                    vm.diffPrevistoMesLA = (vm.qtdePrevistoMesLA - resumo.LA.previsto) / vm.qtdePrevistoMesLA * 100;

                })
                .catch(function(err) {
                    logger.error(err.message);
                });

        }

    }
})();