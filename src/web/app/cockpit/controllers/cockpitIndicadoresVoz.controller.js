(function() {
    'use strict';

    angular
        .module('app.cockpit')
        .controller('cockpitIndicadoresVozController', cockpitIndicadoresVozController);

    cockpitIndicadoresVozController.$inject = ['indicadoresVozService', '$scope', 'logger'];

    function cockpitIndicadoresVozController(indicadoresVozService, $scope, logger) {
        var vm = this;

        //Enviado/Estimado mes
        vm.qtdeEnviadosMes = 0
        vm.diffEnviadosMes = 0
        vm.qtdeEstimadoMes = 0
        vm.diffEstimadoMes = 0

        vm.qtdeEnviadoMesAnterior = 0
        vm.diffEnviadoMesAnterior = 0

        vm.qtdeEstimadoMesAnterior = 0
        vm.diffEstimadoMesAnterior = 0


        //Enviado/Estimado dia
        vm.qtdeEnviadosDia = 0
        vm.qtdeEstimadosDia = 0


        //Atraso de envio
        vm.chamadasAtrasadas = 0
        vm.campanhasAtrasadas = 0
        vm.atrasoMedio = 0
        vm.atrasoMaximo = 0


        //Vazao
        vm.vazao = 0

        vm.chamadasEmCurso = 0
        vm.falhaTts = 0

        //////////////////////////////

        var primeiraCapturaFila = true
        var indiceSeriesFila = {}
        var indiceDistribuicao = {}

        var calcVazao = [{
            enviadosCampanha: 0,
            enviadosAvulso: 0,
            data: new moment()
        }]



        var grafDistStatusDia = null
        var grafDistRotasDia = null
        var grafDistTiposDestinoDia = null

        var grafDistStatusMes = null
        var grafDistRotasMes = null
        var grafDistTiposDestinoMes = null


        var grafRedirecionadosAlgar = null

        var grafFilaEnvio = null

        ///////////////

        function gerarGraficoDistribuicao(idContainer) {
            return Highcharts
                .chart(idContainer, Highcharts.merge({
                    chart: {
                        type: 'bar',
                        backgroundColor: null,
                        borderWidth: 0,
                        margin: [0, 0, 35, 0],
                        padding: 0
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    legend: {
                        margin: 0,
                        floating: true,
                        verticalAlign: 'bottom',
                        y: 10,
                        itemDistance: 0,
                        itemStyle: {
                            fontSize: '13px'
                        }
                    },
                    tooltip: {
                        headerFormat: '',
                        pointFormat: '{series.name}: <b>{point.y} ({point.percentage:.1f}%)</b>'
                    },
                    yAxis: {
                        endOnTick: false,
                        startOnTick: false,
                        labels: {
                            enabled: false
                        },
                        title: {
                            text: null
                        },
                        tickPositions: [0]

                    },
                    xAxis: {
                        labels: {
                            enabled: false
                        },
                        title: {
                            text: null
                        },
                        startOnTick: false,
                        endOnTick: false,
                        tickPositions: []
                    },
                    plotOptions: {
                        bar: {
                            stacking: 'percent',
                        },

                        series: {
                            dataLabels: {
                                enabled: true,
                                align: 'center',
                                color: '#FFFFFF',
                                //format: '{point.percentage:.0f}%',

                                formatter: function() {
                                    if (this.point.percentage.toFixed(1) > 0)
                                        return this.point.percentage.toFixed(1) + '%'
                                },
                                style: {
                                    color: "#FFFFFF",
                                    fontSize: "12px",
                                    fontWeight: "normal",
                                    textOutline: "1px 0px #333333"
                                }
                            },
                        }

                    },
                    series: []
                }))
        }

        setTimeout(function() {
            activate();
        }, 1000);

        ////////////////

        function activate() {
            gerarGraficoFila()
            grafDistStatusDia = gerarGraficoDistribuicao('graf-status-dia')
            grafDistRotasDia = gerarGraficoDistribuicao('graf-rotas-dia')
            grafDistTiposDestinoDia = gerarGraficoDistribuicao('graf-tipos_destino-dia')

            grafDistStatusMes = gerarGraficoDistribuicao('graf-status-mes')
            grafDistRotasMes = gerarGraficoDistribuicao('graf-rotas-mes')
            grafDistTiposDestinoMes = gerarGraficoDistribuicao('graf-tipos_destino-mes')

            grafRedirecionadosAlgar = gerarGraficoDistribuicao('graf-redirecionados-algar')

            atualizarGraficos();
            var intervalo = setInterval(atualizarGraficos, 35 * 1000);

            $scope.$on("$destroy", function() {
                clearInterval(intervalo);
            });

        }


        function atualizarGraficos() {
            atualizarValoresDia();
            atualizarValoresMesCorrente();
            atualizarValoresMesAnterior();
            atualizarValoresInstantaneo();
        }

        function gerarGraficoFila() {
            grafFilaEnvio = Highcharts.chart('graf-fila-envio', {
                chart: {
                    type: 'area'
                },
                exporting: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                xAxis: {
                    type: 'datetime',
                },
                yAxis: {
                    title: {
                        text: 'Na fila'
                    }
                },
                legend: {
                    itemStyle: {
                        fontSize: '20px'
                    }
                },
                plotOptions: {
                    area: {
                        stacking: 'normal',
                        connectNulls: true // this will stop the "serrated" appearance
                    }
                },
                series: []
            });
        }

        function adicionarPontosLinha(linha) {
            var pontos = []
            var dataCaptura = Date.now();

            for (var i in indiceSeriesFila) {
                pontos[indiceSeriesFila[i]] = 0
            }

            for (var grupo in linha.capturas) {

                if (typeof indiceSeriesFila[grupo] == 'undefined') {
                    indiceSeriesFila[grupo] = Object.keys(indiceSeriesFila).length
                    grafFilaEnvio.addSeries({
                        name: grupo
                    }, false)
                }

                pontos[indiceSeriesFila[grupo]] = linha.capturas[grupo]
            }

            for (var i in pontos) {
                grafFilaEnvio.series[i].addPoint(
                    [
                        moment(linha.data).valueOf(),
                        parseFloat(pontos[i])
                    ], false
                );
            }
        }


        function atualizarPontosDistribuicao(linha, nomeAtributoArupador, indicesSeries, grafico, atributoValor) {
            var pontos = []
            var atributoValor = atributoValor || 'chamadas'

            if (typeof indiceDistribuicao[indicesSeries] == 'undefined') {
                indiceDistribuicao[indicesSeries] = {}
            }

            for (var indiceAgrupador in indiceDistribuicao[indicesSeries]) {
                pontos[indiceDistribuicao[indicesSeries][indiceAgrupador]] = 0

                if (grafico.series.length - 1 < indiceDistribuicao[indicesSeries][indiceAgrupador]) {
                    grafico.addSeries({
                        name: indiceAgrupador,
                        data: [0]
                    }, false)
                }
            }

            //Verifica se entrou uma nova serie no indice de series


            for (var i in linha) {
                if (linha[i][atributoValor] == 0 || linha[i][atributoValor] == null) {
                    continue;
                }

                if (typeof indiceDistribuicao[indicesSeries][linha[i][nomeAtributoArupador]] == 'undefined') {
                    indiceDistribuicao[indicesSeries][linha[i][nomeAtributoArupador]] = Object.keys(indiceDistribuicao[indicesSeries]).length
                    grafico.addSeries({
                        name: linha[i][nomeAtributoArupador],
                        data: [0]
                    }, false)
                }

                pontos[indiceDistribuicao[indicesSeries][linha[i][nomeAtributoArupador]]] = linha[i][atributoValor]
            }

            for (var i in pontos) {
                grafico.series[i].update({
                    data: [pontos[i]]
                })
            }

            grafico.redraw()
        }

        function atualizarValoresInstantaneo() {
            var inicioPeriodo = moment().subtract(1, 'hour').toDate();
            var fimPeriodo = moment().endOf('day').toDate();

            indicadoresVozService
                .getFila(inicioPeriodo, fimPeriodo)
                .then(function(resumo) {
                    if (primeiraCapturaFila) {
                        for (var linha in resumo) {
                            adicionarPontosLinha(resumo[linha])
                        }

                    } else {
                        adicionarPontosLinha(resumo[0])
                    }
                    grafFilaEnvio.redraw()
                    primeiraCapturaFila = false
                })
                .catch(function(err) {
                    logger.error(err.message);
                });


            indicadoresVozService
                .getVazao()
                .then(function(vazao) {
                    vm.vazao = vazao * 60 * 60
                })
                .catch(function(err) {
                    logger.error(err.message);
                });


            indicadoresVozService
                .getAtraso()
                .then(function(atraso) {
                    vm.atrasoMaximo = atraso.atraso_maximo
                    vm.chamadasAtrasadas = atraso.atrasados
                    vm.campanhasAtrasadas = atraso.campanhas_atrasadas
                    vm.atrasoMedio = atraso.atraso_medio
                })
                .catch(function(err) {
                    logger.error(err.message);
                });

            indicadoresVozService
                .getChamadasEmCurso(inicioPeriodo, fimPeriodo)
                .then(function(capturas) {
                    vm.chamadasEmCurso = capturas[capturas.length - 1].em_curso
                })
                .catch(function(err) {
                    logger.error(err.message);
                });


        }

        function atualizarValoresDia() {
            var inicioDia = moment().startOf('day').toDate();
            var fimDia = moment().endOf('day').toDate();

            indicadoresVozService
                .getResumo(inicioDia, fimDia, 'status')
                .then(function(resumo) {
                    atualizarPontosDistribuicao(resumo, 'status', 'status', grafDistStatusDia)
                })
                .catch(function(err) {
                    logger.error(err.message);
                });

            indicadoresVozService
                .getResumo(inicioDia, fimDia, 'rota')
                .then(function(resumo) {
                    atualizarPontosDistribuicao(resumo, 'rota', 'rotas', grafDistRotasDia)
                    atualizarPontosDistribuicao(resumo, 'rota', 'redirecionados-algar', grafRedirecionadosAlgar, 'redirecionados_algar')
                })
                .catch(function(err) {
                    logger.error(err.message);
                });

            indicadoresVozService
                .getResumo(inicioDia, fimDia, 'tipo_destino')
                .then(function(resumo) {

                    var total = 0
                    var falhaTts = 0
                    for (var i in resumo) {
                        total += resumo[i].chamadas
                        falhaTts += resumo[i].falhas_tts
                    }
                    vm.qtdeEnviadosDia = total
                    vm.falhaTts = falhaTts

                    atualizarPontosDistribuicao(resumo, 'tipo_destino', 'tipo_destino', grafDistTiposDestinoDia)
                })
                .catch(function(err) {
                    logger.error(err.message);
                });

            indicadoresVozService
                .getPrevisao(inicioDia, fimDia)
                .then(function(previsao) {
                    vm.qtdeEstimadosDia = previsao

                })
                .catch(function(err) {
                    logger.error(err.message);
                });

        }



        function atualizarValoresMesCorrente() {
            var inicioMes = moment().startOf('month').toDate();
            var fimMes = moment().endOf('day').toDate();

            indicadoresVozService
                .getResumo(inicioMes, fimMes, 'status')
                .then(function(resumo) {
                    atualizarPontosDistribuicao(resumo, 'status', 'status', grafDistStatusMes)
                })
                .catch(function(err) {
                    logger.error(err.message);
                });

            indicadoresVozService
                .getResumo(inicioMes, fimMes, 'rota')
                .then(function(resumo) {
                    atualizarPontosDistribuicao(resumo, 'rota', 'rotas', grafDistRotasMes)
                })
                .catch(function(err) {
                    logger.error(err.message);
                });

            indicadoresVozService
                .getResumo(inicioMes, fimMes, 'tipo_destino')
                .then(function(resumo) {

                    var total = 0
                    for (var i in resumo) {
                        total += resumo[i].chamadas
                    }
                    vm.qtdeEnviadosMes = total

                    atualizarPontosDistribuicao(resumo, 'tipo_destino', 'tipo_destino', grafDistTiposDestinoMes)
                })
                .catch(function(err) {
                    logger.error(err.message);
                });

            indicadoresVozService
                .getPrevisao(inicioMes, fimMes)
                .then(function(previsao) {
                    vm.qtdeEstimadoMes = previsao || 0
                })
                .catch(function(err) {
                    logger.error(err.message);
                });

        }

        function atualizarValoresMesAnterior() {
            var inicioMes = moment().add(-1, 'month').startOf(1, 'month').toDate();
            var fimMes = moment().add(-1, 'month').toDate();

            indicadoresVozService
                .getResumo(inicioMes, fimMes)
                .then(function(resumo) {
                    vm.qtdeEnviadoMesAnterior = resumo.chamadas || 0
                    vm.diffEnviadoMesAnterior = vm.qtdeEnviadosMes > 0 ? (vm.qtdeEnviadosMes - vm.qtdeEnviadoMesAnterior) / vm.qtdeEnviadosMes * 100 : 0
                })
                .catch(function(err) {
                    logger.error(err.message);
                });


            indicadoresVozService
                .getPrevisao(inicioMes, fimMes)
                .then(function(previsao) {
                    vm.qtdeEstimadoMesAnterior = previsao || 0
                    vm.diffEstimadoMesAnterior = vm.qtdeEstimadoMes > 0 ? (vm.qtdeEstimadoMes - vm.qtdeEstimadoMesAnterior) / vm.qtdeEstimadoMes * 100 : 0


                })
                .catch(function(err) {
                    logger.error(err.message);
                });

        }
    }
})();