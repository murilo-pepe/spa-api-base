(function() {
    'use strict';
    angular
        .module('app.cockpit')
        .controller("cockpitIndiceHorasProjetosController", cockpitIndiceHorasProjetosController);

    cockpitIndiceHorasProjetosController.$inject = ["indiceHorasProjetosService", '$scope', 'logger'];

    function cockpitIndiceHorasProjetosController(indiceHorasProjetosService, $scope, logger) {

        var gaugeOptions = {
            exporting: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            chart: {
                type: 'solidgauge'
            },
            title: null,
            pane: {
                center: ['50%', '90%'],
                size: '160%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: '#EEE',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },
            tooltip: {
                enabled: false
            },
            // the value axis
            yAxis: {
                min: 0,
                max: 100,
                title: {
                    // text: '% lançamento de horas'
                },
                stops: [
                    [0.74, '#DD0000'],
                    [0.84, '#FF731C'],
                    [0.94, '#DDDF0D'],
                    [1, '#55BF3B'],
                    [1.01, '#993399']
                ],
                lineWidth: 0,
                minorTickInterval: null,
                tickAmount: 2,
                title: {
                    y: -70
                },
                labels: {
                    y: 16
                }
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 15,
                        borderWidth: 0,
                        useHTML: true
                    }
                }
            }
        };

        ////////////////

        var vm = this;

        vm.total_sem_estimativa = 0;
        vm.mediaAtrasoMes = 0;
        vm.graficoEstimadoRealizado = {}
        vm.grafEntreguesNoPrazo = {}

        setTimeout(function() {
            activate();
        }, 1000);
        //////////////

        function activate() {
            atualizarGraficos();
            var intervalo = setInterval(atualizarGraficos, 3000);

            $scope.$on("$destroy", function() {
                clearInterval(intervalo);
            });

            vm.graficoEstimadoRealizado = gerarGrafico('graf-horas-estimadas-realizadas', 'Realizado')
            vm.grafEntreguesNoPrazo = gerarGrafico('graf-procent-demandas-no-prazo', 'No prazo')

        }


        function gerarGrafico(idContainer, label) {
            return Highcharts
                .chart(idContainer, Highcharts.merge(gaugeOptions, {
                    series: [{
                        data: [0],
                        dataLabels: {
                            format: '<div style="text-align:center"><span style="font-size:48px;color:black">{y}</span><br/>' +
                                '<span style="font-size:20px;color:gray">% ' + label + '</span></div>'
                        },
                    }]
                }))
        }

        function atualizarGraficos() {

            indiceHorasProjetosService
                .getHorasEstimadasRealizadas()
                .then(function(valor) {
                    vm.total_sem_estimativa = valor['total_sem_estimativa'];
                    vm.graficoEstimadoRealizado.series[0].points[0]
                        .update(valor['calculado']);
                })
                .catch(function(err) {
                    logger.error(err.message);
                });

            indiceHorasProjetosService
                .getDemandasEntreguesNoPrazo()
                .then(function(valor) {
                    vm.grafEntreguesNoPrazo.series[0].points[0]
                        .update(Math.floor(valor));
                })
                .catch(function(err) {
                    logger.error(err.message);
                });

            indiceHorasProjetosService
                .getMediaAtrasoDemadas()
                .then(function(valor) {
                    vm.mediaAtrasoMes = valor
                })
                .catch(function(err) {
                    logger.error(err.message);
                });
        }

    }

})();