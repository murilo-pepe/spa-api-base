(function() {
    'use strict';
    angular
        .module('app.cockpit')
        .controller("cockpitLancamentoHorasController", cockpitLancamentoHorasController);

    cockpitLancamentoHorasController.$inject = ["lancamentoHorasService", '$scope', 'logger'];

    function cockpitLancamentoHorasController(lancamentoHorasService, $scope, logger) {

        var gaugeOptions = {
            exporting: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            chart: {
                type: 'solidgauge'
            },
            title: null,
            pane: {
                center: ['50%', '90%'],
                size: '160%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: '#EEE',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },
            tooltip: {
                enabled: false
            },
            // the value axis
            yAxis: {
                min: 0,
                max: 100,
                title: {
                    // text: '% lançamento de horas'
                },
                stops: [
                    [0.74, '#DD0000'],
                    [0.84, '#FF731C'],
                    [0.94, '#DDDF0D'],
                    [1, '#55BF3B'],
                    [1.01, '#993399']
                ],
                lineWidth: 0,
                minorTickInterval: null,
                tickAmount: 2,
                title: {
                    y: -70
                },
                labels: {
                    y: 16
                }
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 15,
                        borderWidth: 0,
                        useHTML: true
                    }
                }
            }
        };

        ////////////////

        var vm = this;
        vm.lancamentos = [];

        vm.times = [];

        vm.graficosLancamento = [];

        vm.graficosLancamento_totais = {}

        setTimeout(function() {
            activate();
        }, 1000);
        //////////////

        function activate() {
            atualizarGraficosLancamento();
            var intervalo = setInterval(atualizarGraficosLancamento, 15000);

            $scope.$on("$destroy", function() {
                clearInterval(intervalo);
            });


        }

        function iniciarGraficosLancamento(idx) {
            return {
                semana: gerarGrafico('graf-horas-semana-' + idx, 'Semana'),
                mes: gerarGrafico('graf-horas-mes-' + idx, 'Mês'),
                dia: gerarGrafico('graf-horas-dia-' + idx, 'Dia')
            }
        }

        function gerarGrafico(idContainer, label) {
            return Highcharts
                .chart(idContainer, Highcharts.merge(gaugeOptions, {
                    series: [{
                        data: [0],
                        dataLabels: {
                            format: '<div style="text-align:center"><span style="font-size:32px;color:black">{y}</span><br/>' +
                                '<span style="font-size:18px;color:gray">% ' + label + '</span></div>'
                        },
                    }]
                }))
        }

        function atualizarGraficosLancamento() {

            lancamentoHorasService
                .getIndiceLancamento()
                .then(function(dados) {

                    var totais = {
                        dia: 0,
                        semana: 0,
                        mes: 0
                    };

                    for (var idxTime in dados) {
                        if (typeof vm.times[idxTime] == 'undefined') {
                            vm.times[idxTime] = dados[idxTime].time;
                        }

                        vm.lancamentos[idxTime] = dados[idxTime];
                        totais.dia += dados[idxTime].dia;
                        totais.semana += dados[idxTime].semana;
                        totais.mes += dados[idxTime].mes;

                        setTimeout(function(idxTime) {

                            if (typeof vm.graficosLancamento[vm.lancamentos[idxTime].time] == 'undefined') {
                                vm.graficosLancamento[vm.lancamentos[idxTime].time] = iniciarGraficosLancamento(vm.lancamentos[idxTime].time);
                            }

                            vm.graficosLancamento[vm.lancamentos[idxTime].time].dia.series[0].points[0]
                                .update(vm.lancamentos[idxTime].dia);

                            vm.graficosLancamento[vm.lancamentos[idxTime].time].mes.series[0].points[0]
                                .update(vm.lancamentos[idxTime].mes);

                            vm.graficosLancamento[vm.lancamentos[idxTime].time].semana.series[0].points[0]
                                .update(vm.lancamentos[idxTime].semana);

                        }, 500, idxTime)
                    }



                    setTimeout(function(totais) {
                        if (typeof vm.graficosLancamento_totais.dia == 'undefined') {
                            vm.graficosLancamento_totais = iniciarGraficosLancamento('geral');
                        }

                        vm.graficosLancamento_totais.dia.series[0].points[0]
                            .update(Math.floor(totais.dia / vm.lancamentos.length));

                        vm.graficosLancamento_totais.mes.series[0].points[0]
                            .update(Math.floor(totais.mes / vm.lancamentos.length));

                        vm.graficosLancamento_totais.semana.series[0].points[0]
                            .update(Math.floor(totais.semana / vm.lancamentos.length));
                    }, 500, totais)

                })
                .catch(function(err) {
                    logger.error(err.message);
                });



        }

    }

})();