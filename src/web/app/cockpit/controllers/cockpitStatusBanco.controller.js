(function() {
    'use strict';
    angular
        .module('app.cockpit')
        .controller("cockpitStatusBancoController", cockpitStatusBancoController);

    cockpitStatusBancoController.$inject = ["databaseStatusService", '$scope', 'logger'];

    function cockpitStatusBancoController(databaseStatusService, $scope, logger) {

        //////////////

        var vm = this;
        var primeiraConsulta = true

        vm.grafConexoes = null
        vm.grafQueriesPs = null
        vm.grafQueriesLentas = null


        //////////////////
        setTimeout(function() {

            activate();
        }, 1000);

        function activate() {
            vm.grafQueriesLentas = gerarGraficoLinha('graf-queries-lentas', 'Queries lentas')
            vm.grafQueriesPs = gerarGraficoLinha('graf-queries-ps', 'Queries/s')
            vm.grafConexoes = gerarGraficoLinha('graf-conexoes', 'Conexões')

            getStatusBancos();

            var intervalo = setInterval(getStatusBancos, 10000);
            $scope.$on("$destroy", function() {
                clearInterval(intervalo);
            });
        }

        function gerarGraficoLinha(elemento, titulo) {
            return Highcharts.chart(elemento, {
                chart: {
                    zoomType: 'x'
                },
                plotOptions: {
                    line: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                legend: {
                    itemStyle: {
                        fontSize: '20px'
                    }
                },
                title: {
                    text: ''
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: titulo
                    }
                },
                series: []
            })
        }

        function getStatusBancos() {
            var dataI = moment().toDate()
            return databaseStatusService
                .getHistoricoStatusAll(dataI)
                .then(function(dados) {
                    vm.statusBancos = {}
                    for (var line in dados) {
                        vm.statusBancos[line] = dados[line][dados[line].length - 1]
                    }

                    atualizarGraficos(dados, !primeiraConsulta);
                    primeiraConsulta = false
                })
                .catch(function(err) {
                    logger.error(err.message);
                });

        }

        function atualizarGraficos(dados, apenasUltimoRegistro) {
            var i = 0;

            for (var banco in dados) {

                if (typeof vm.grafConexoes.series[i] == 'undefined') {
                    vm.grafConexoes.addSeries({
                        name: banco,
                        data: []
                    })
                    vm.grafQueriesPs.addSeries({
                        name: banco,
                        data: []
                    })
                    vm.grafQueriesLentas.addSeries({
                        name: banco,
                        data: []
                    })
                }

                if (vm.grafConexoes.series[i].data.length > 360) { //Equivalente a 1h

                    vm.grafConexoes.series[i].removePoint(0)
                    vm.grafQueriesPs.series[i].removePoint(0)
                    vm.grafQueriesLentas.series[i].removePoint(0)

                }

                if (!apenasUltimoRegistro) {
                    for (var line in dados[banco]) {

                        var data = moment(dados[banco][line].timestamp).valueOf()
                        vm.grafConexoes.series[i].addPoint([data, parseFloat(dados[banco][line].connections)], false)
                        vm.grafQueriesPs.series[i].addPoint([data, parseFloat(dados[banco][line].queries_ps)], false)
                        vm.grafQueriesLentas.series[i].addPoint([data, parseFloat(dados[banco][line].slow_queries)], false)

                    }
                    vm.grafQueriesPs.redraw()
                    vm.grafQueriesLentas.redraw()
                    vm.grafConexoes.redraw()
                } else {

                    var data = moment(dados[banco][dados[banco].length - 1].timestamp).valueOf()

                    vm.grafConexoes.series[i].addPoint([data, parseFloat(dados[banco][dados[banco].length - 1].connections)])
                    vm.grafQueriesPs.series[i].addPoint([data, parseFloat(dados[banco][dados[banco].length - 1].queries_ps)])
                    vm.grafQueriesLentas.series[i].addPoint([data, parseFloat(dados[banco][dados[banco].length - 1].slow_queries)])

                }



                i++;
            }

        }

    }

})();