(function () {
    'use strict';

    angular
        .module('app.cockpit')
        .filter('numberToPower', function () {
            return function (value, precision) {


                if (isNaN(parseFloat(value)) || !isFinite(value))
                    return '-';

                if(value == 0 ){
                    return '0';
                }

                if (typeof precision === 'undefined')
                    precision = 1;

                var units = ['', 'K', 'M', 'G', 'T', 'P'];
                var number = Math.floor(Math.log(value) / Math.log(1000));

                return (value / Math.pow(1000, Math.floor(number))).toFixed(precision) +
                    ' ' +
                    units[number];
            }
        });

})();