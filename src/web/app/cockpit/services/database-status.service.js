(function () {
    'use strict';

    angular
            .module('app.cockpit')
            .service("databaseStatusService", databaseStatusService);

    databaseStatusService.$inject = ['$http', 'api', '$q'];

    function databaseStatusService($http, api, $q) {

        this.getStatusAll = function (params) {
            
            return $http
                    .get(api.url + 'bd-status', {})
                    .then(function (result) {
                        return result.data;
                    })
                    .catch(function (parameters) {
                        return Promise.reject(parameters.data);
                    });

        };

        this.getHistoricoStatusAll = function (params) {
            return $http
                    .get(api.url + 'bd-status', {data_inicio: params.data_inicio}).
                    then(function (result) {
                        return result.data;
                    })
                    .catch(function (parameters) {
                        return Promise.reject(parameters.data);
                    });

        };
    }
})();

