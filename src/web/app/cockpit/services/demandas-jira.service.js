(function () {
    'use strict';

    angular
        .module('app.cockpit')
        .service("demandasJiraService", demandasJiraService);

    demandasJiraService.$inject = ['$http', 'api', '$q'];

    function demandasJiraService($http, api, $q) {

        this.listaDemandas = function (dataInicial, dataFinal, groupBy, projeto) {


            return $q(function (resolve, reject) {
                $http
                    .get(api.url + 'demandas', {
                        params: {
                            dataInicial: dataInicial,
                            dataFinal: dataFinal,
                            groupBy: groupBy,
                            projeto: projeto
                        }
                    })
                    .then(function (result) {
                        var resultado = result.data

                        resolve(resultado)

                    })
                    .catch(function (parameters) {
                        return Promise.reject(parameters.data);
                    });

            })


        };
    }
})();