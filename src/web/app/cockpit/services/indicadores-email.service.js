(function () {
    'use strict';

    angular
        .module('app.cockpit')
        .service('indicadoresEmailService', indicadoresEmailService);

    indicadoresEmailService.$inject = ['$http', 'api', '$q'];

    function indicadoresEmailService($http, api, $q) {
        var srv = this

        srv.getFila = function (dataInicial, dataFinal) {
            return $q(function (resolve, reject) {
                $http
                    .get(api.url + 'email/fila', {
                        params: {
                            dataInicial: dataInicial,
                            dataFinal: dataFinal
                        }
                    })
                    .then(function (result) {
                        return resolve(result.data);
                    })
                    .catch(function (parameters) {
                        return reject(parameters.data);
                    });;
            })
        }


        srv.getResumo = function (dataInicial, dataFinal) {
            return $q(function (resolve, reject) {
                $http
                    .get(api.url + 'email/resumo-periodo', {
                        params: {
                            dataInicial: dataInicial,
                            dataFinal: dataFinal
                        }
                    })
                    .then(function (result) {
                        return resolve(result.data);
                    })
                    .catch(function (parameters) {
                        return reject(parameters.data);
                    });
            })
        }

        srv.getPrevisao = function (dataInicial, dataFinal) {
            return $q(function (resolve, reject) {
                $http
                    .get(api.url + 'email/previsao', {
                        params: {
                            dataInicial: dataInicial,
                            dataFinal: dataFinal
                        }
                    })
                    .then(function (result) {
                        return resolve(result.data);
                    })
                    .catch(function (parameters) {
                        return reject(parameters.data);
                    });
            })
        }

        srv.getEstimativa = function (dataInicial, dataFinal) {
            return $q(function (resolve, reject) {
                $http
                    .get(api.url + 'email/estimativa', {
                        params: {
                            dataInicial: dataInicial,
                            dataFinal: dataFinal
                        }
                    })
                    .then(function (result) {
                        return resolve(result.data);
                    })
                    .catch(function (parameters) {
                        return reject(parameters.data);
                    });
            })
        }
    }
})();