(function () {
    'use strict';

    angular
        .module('app.cockpit')
        .service("indicadoresSmsService", indicadoresSmsService);

    indicadoresSmsService.$inject = ['$http', 'api', '$q'];

    function indicadoresSmsService($http, api, $q) {
        var srv = this

        srv.getResumoPeriodo = function(dataInicial, dataFinal) {
            return $q(function (resolve, reject) {
                $http
                    .get(api.url + '/sms-resumo-campanhas', {
                        params: {
                            dataInicial: dataInicial,
                            dataFinal: dataFinal
                        }
                    })
                    .then(function (result) {
                        return resolve(result.data);
                    })
                    .catch(function (parameters) {
                        return reject(parameters.data);
                    });;
            })
        }
    }
})();