(function () {
    'use strict';

    angular
        .module('app.cockpit')
        .service('indicadoresVozService', indicadoresVozService);

    indicadoresVozService.$inject = ['$http', 'api', '$q'];

    function indicadoresVozService($http, api, $q) {
        var srv = this

        srv.getFila = function (dataInicial, dataFinal) {
            return $q(function (resolve, reject) {
                $http
                    .get(api.url + 'voz/snapshot-fila', {
                        params: {
                            dataInicial: dataInicial,
                            dataFinal: dataFinal
                        }
                    })
                    .then(function (result) {
                        return resolve(result.data);
                    })
                    .catch(function (parameters) {
                        return reject(parameters.data);
                    });;
            })
        }

        srv.getAtraso = function () {
            return $q(function (resolve, reject) {
                $http
                    .get(api.url + 'voz/atraso-atual')
                    .then(function (result) {
                        return resolve(result.data);
                    })
                    .catch(function (parameters) {
                        return reject(parameters.data);
                    });;
            })
        }

        srv.getChamadasEmCurso = function (dataInicial, dataFinal) {
            return $q(function (resolve, reject) {
                $http
                    .get(api.url + 'voz/chamadas-em-curso', {
                        params: {
                            dataInicial: dataInicial,
                            dataFinal: dataFinal,
                        }
                    })
                    .then(function (result) {
                        return resolve(result.data);
                    })
                    .catch(function (parameters) {
                        return reject(parameters.data);
                    });;
            })
        }

        srv.getResumo = function (dataInicial, dataFinal, agrupamento) {
            return $q(function (resolve, reject) {
                $http
                    .get(api.url + 'voz/resumo-periodo', {
                        params: {
                            dataInicial: dataInicial,
                            dataFinal: dataFinal,
                            groupBy: agrupamento
                        }
                    })
                    .then(function (result) {
                        return resolve(result.data);
                    })
                    .catch(function (parameters) {
                        return reject(parameters.data);
                    });
            })
        }

        srv.getVazao = function (dataInicial, dataFinal, agrupamento) {
            return $q(function (resolve, reject) {
                $http
                    .get(api.url + 'voz/vazao-atual')
                    .then(function (result) {
                        return resolve(result.data);
                    })
                    .catch(function (parameters) {
                        return reject(parameters.data);
                    });
            })
        }

        srv.getPrevisao = function (dataInicial, dataFinal) {
            return $q(function (resolve, reject) {
                $http
                    .get(api.url + 'voz/previsao-periodo', {
                        params: {
                            dataInicial: dataInicial,
                            dataFinal: dataFinal
                        }
                    })
                    .then(function (result) {
                        return resolve(result.data);
                    })
                    .catch(function (parameters) {
                        return reject(parameters.data);
                    });
            })
        }
    }
})();