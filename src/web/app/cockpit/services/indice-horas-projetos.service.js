(function () {
    'use strict';

    angular
        .module('app.cockpit')
        .service("indiceHorasProjetosService", indiceHorasProjetosService);

    indiceHorasProjetosService.$inject = ['$http', 'api', '$q'];

    function indiceHorasProjetosService($http, api, $q) {

        this.getHorasEstimadasRealizadas = function (prams) {
            return $http
                .get(api.url + 'indice-horas-projetos', {})
                .then(function (result) {
                    result.data['calculado'] = Math.round(result.data['trabalhado'] / result.data['estimado'] * 100);
                    return result.data;
                })
                .catch(function (parameters) {
                    return Promise.reject(parameters.data);
                });
        }

        this.getDemandasEntreguesNoPrazo = function () {
            return $http
                .get(api.url + 'porcentagem-demandas-entregues-no-prazo')
                .then(function (result) {
                    return result.data;
                })
                .catch(function (parameters) {
                    return Promise.reject(parameters.data);
                });
        }

        this.getMediaAtrasoDemadas = function () {
            return $http
                .get(api.url + 'media-atraso-demandas')
                .then(function (result) {
                    return result.data;
                })
                .catch(function (parameters) {
                    return Promise.reject(parameters.data);
                });
        }

        this.getFaturamentoClientes = function (dataInicial,dataFinal) {
            return $q(function (resolve, reject) {
                $http
                    .get(api.url + 'demandas/faturamento-clientes', {
                        params: {
                            dataInicial: dataInicial,
                            dataFinal: dataFinal
                        }
                    })
                    .then(function (result) {
                        return resolve(result.data);
                    })
                    .catch(function (parameters) {
                        return reject(parameters.data);
                    });
            })
        }
    }
})();