(function () {
    'use strict';

    angular
        .module('app.cockpit')
        .service("lancamentoHorasService", lancamentoHorasService);

    lancamentoHorasService.$inject = ['$http', 'api', '$q'];

    function lancamentoHorasService($http, api, $q) {

        this.getIndiceLancamento = function (params) {

            return $q(function (resolve, reject) {
                $http
                    .get(api.url + 'lancamento-horas', {})
                    .then(function (result) {
                        var times = []

                        for (var index in result.data) {
                            if (typeof times[result.data[index].grupo] == 'undefined') {
                                times[result.data[index].grupo] = {
                                    time: result.data[index].grupo,
                                    semana: 0,
                                    mes: 0,
                                    dia: 0
                                }
                            }

                            switch (result.data[index].periodo) {
                                case 'mes':
                                    times[result.data[index].grupo].mes = Math.floor(result.data[index].total_logado / result.data[index].total_previsto * 100)
                                    break;

                                case 'semana':
                                    times[result.data[index].grupo].semana = Math.floor(result.data[index].total_logado / result.data[index].total_previsto * 100)
                                    break;

                                case 'dia':
                                    times[result.data[index].grupo].dia = Math.floor(result.data[index].total_logado / result.data[index].total_previsto * 100)
                                    break;
                            }
                        }

                        //Transforma em array ao invés de objeto para facilitar a renderização da view
                        var arr = Object.keys(times).map(function (key) {
                            return times[key];
                        });
                        resolve(arr)

                    })
                    .catch(function (parameters) {
                        return Promise.reject(parameters.data);
                    });

            })


        };
    }
})();