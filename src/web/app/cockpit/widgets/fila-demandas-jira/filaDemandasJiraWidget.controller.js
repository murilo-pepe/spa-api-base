(function() {
    'use strict';

    angular
        .module('app.cockpit')
        .controller('filaDemandasJiraWidgetController', filaDemandasJiraWidgetController);

    filaDemandasJiraWidgetController.$inject = ['$scope', 'demandasJiraService'];

    function filaDemandasJiraWidgetController($scope, demandasJiraService) {
        var vm = this;
        var chart
        var widgetOptions = $scope.widget.options

        vm.demandas = []

        activate();

        ////////////////
        function activate() {
            setTimeout(function() {
                criarGrafico()
                atualizarDemandas()


            }, 1000)

            setInterval(atualizarDemandas, 10 * 1000)
        }


        function criarGrafico() {
            chart = Highcharts.chart('container_' + $scope.widget.id, {
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    type: 'category',
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Demandas'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
  
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Demandas',
                    data: []
                }]
            });

        }

        function atualizarDemandas() {
            var horas = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
            var promises = []


            for (var i in horas) {
                promises.push(
                    demandasJiraService.listaDemandas(
                        moment().hour(horas[i]).startOf('hour').toDate(),
                        moment().hour(horas[i]).endOf('hour').toDate(),
                        'status',
                        widgetOptions.projeto
                    )

                )
            }

            Promise
                .all(promises)
                .then(function(demandasHora) {
                    var dadosGraf = []


                    for (var i in demandasHora) {
                        var total = 0

                        _.forEach(demandasHora[i], function(linha) {
                            if (_.indexOf(widgetOptions.status, linha.status) != -1) {
                                total += linha.demandas
                            }
                        })

                        dadosGraf.push([(horas[i] + 3) + ':00', total])
                    }

                    chart.series[0].setData(dadosGraf)
                    chart.redraw();
                    chart.reflow();


                })


        }

    }

})();