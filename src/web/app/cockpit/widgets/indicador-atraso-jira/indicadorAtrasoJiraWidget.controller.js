(function() {
    'use strict';

    angular
        .module('app.cockpit')
        .controller('indicadorAtrasoJiraWidgetController', indicadorAtrasoJiraWidgetController);

    indicadorAtrasoJiraWidgetController.$inject = ['$scope', 'demandasJiraService'];

    function indicadorAtrasoJiraWidgetController($scope, demandasJiraService) {
        var vm = this;
        vm.valor = 0;
        var widgetOptions = $scope.widget.options


        activate();

        ////////////////
        function activate() {
            setTimeout(function() {
                atualizarDemandas()


            }, 1000)

            setInterval(atualizarDemandas, 10 * 1000)
        }



        function atualizarDemandas() {

            var consulta = demandasJiraService
                .listaDemandas(
                    moment().startOf('day').toDate(),
                    moment().endOf('day').toDate(),
                    'status',
                    widgetOptions.projeto
                )

            if (widgetOptions.tipo == 'max') {
                consulta
                    .then(function(demandasHora) {
                        _.forEach(demandasHora, function(linha) {

                            if (_.indexOf(widgetOptions.status, linha.status) != -1) {
                                vm.valor = Math.max(linha.tempo_maximo, vm.valor)
                            }

                        })
                        $(window).trigger('resize')

                    })
            } else {
                consulta
                    .then(function(demandasHora) {
                        var total = 0
                        var demandas = 0
                        _.forEach(demandasHora, function(linha) {
                            if (_.indexOf(widgetOptions.status, linha.status) != -1) {
                                total += linha.tempo_medio
                                demandas += linha.demandas
                            }
                        })

                        vm.valor = demandas > 0 ? (total / demandas) : 0
                        $(window).trigger('resize')

                    })
            }

        }

    }

})();