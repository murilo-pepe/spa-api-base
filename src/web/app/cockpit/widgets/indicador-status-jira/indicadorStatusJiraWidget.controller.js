(function() {
    'use strict';

    angular
        .module('app.cockpit')
        .controller('indicadorStatusJiraWidgetController', indicadorStatusJiraWidgetController);

    indicadorStatusJiraWidgetController.$inject = ['$scope', 'demandasJiraService'];

    function indicadorStatusJiraWidgetController($scope, demandasJiraService) {
        var vm = this;
        vm.valor = 0;
        var widgetOptions = $scope.widget.options


        activate();

        ////////////////
        function activate() {
            setTimeout(function() {
                atualizarDemandas()


            }, 1000)

            setInterval(atualizarDemandas, 10 * 1000)
        }



        function atualizarDemandas() {

            demandasJiraService.listaDemandas(
                    moment().startOf('day').toDate(),
                    moment().endOf('day').toDate(),
                    'status',
                    widgetOptions.projeto
                )


                .then(function(demandasHora) {
                    vm.valor = 0
                    _.forEach(demandasHora, function(linha) {
                        if (_.indexOf(widgetOptions.status, linha.status) != -1) {
                            vm.valor += linha.demandas
                        }
                    })


                })


        }

    }

})();