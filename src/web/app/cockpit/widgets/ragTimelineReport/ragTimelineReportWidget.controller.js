(function() {
    'use strict';

    angular
        .module('app.cockpit')
        .controller('ragTimelineReportWidgetController', ragTimelineReportWidgetController);

    ragTimelineReportWidgetController.$inject = ['$scope', 'demandasJiraService'];

    function ragTimelineReportWidgetController($scope, demandasJiraService) {
        var vm = this;
        var widgetOptions = $scope.widget.options

        ///////////////////

        vm.dias = _.range(1, 32)
        vm.diaHoje = moment().date()
        vm.diaSelecionado = vm.diaHoje
        vm.dadosDias = []
        vm.dadosTotal = {}
        vm.detalhes = []
        vm.defaultColor = 'lightgray'

        vm.randomizarDados = function() {
            for (var i = 0; i < vm.diaHoje; i++) {
                var linha = {
                    projetado: avaliarMetrica('projetado', Math.random() * 30000),
                    custo: avaliarMetrica('custo', Math.random() * 30000),
                    vazao: avaliarMetrica('vazao', Math.random() * 30000),
                    lucratividade: avaliarMetrica('lucratividade', Math.random() * 30000),
                }

                vm.dadosDias.push(linha)
            }

            totalizarDados()
        }

        vm.setDetalhes = function(metrica, dia) {
            var detalhes = []

            if (dia == null) {
                detalhes = vm.dadosTotal[metrica]
            } else {
                if (typeof vm.dadosDias[dia - 1] == 'undefined') {
                    vm.detalhes = []
                    return false
                }
                detalhes = vm.dadosDias[dia - 1][metrica]
            }



            vm.detalhes = [{
                    titulo: 'Rota A',
                    valor: detalhes.valor / 100 * 10
                },
                {
                    titulo: 'Rota B',
                    valor: detalhes.valor / 100 * 20
                },
                {
                    titulo: 'Rota C',
                    valor: detalhes.valor / 100 * 20
                },
                {
                    titulo: 'Rota D',
                    valor: detalhes.valor / 100 * 15
                },
                {
                    titulo: 'Rota E',
                    valor: detalhes.valor / 100 * 15
                },
                {
                    titulo: 'Rota F',
                    valor: detalhes.valor / 100 * 20
                },
            ]

        }

        ////////////////


        ////////////////

        function activate() {
            vm.randomizarDados()
        }

        function avaliarMetrica(metrica, valor) {
            var retorno = {
                metrica: metrica,
                valor: valor,
                status: 'success',
                color: '#009200'
            }

            const statusWarning = 'warning'
            const colorWarning = 'orange'


            const statusDanger = 'danger'
            const colorDanger = 'crimson'

            switch (metrica) {
                case 'projetado':
                    if (valor > 10000) {
                        break;
                    }

                    if (valor > 5000) {
                        retorno.status = statusWarning
                        retorno.color = colorWarning
                        break;
                    }

                    retorno.status = statusDanger
                    retorno.color = colorDanger

                case 'custo':
                    if (valor > 10000) {
                        break;
                    }

                    if (valor > 5000) {
                        retorno.status = statusWarning
                        retorno.color = colorWarning
                        break;
                    }

                    retorno.status = statusDanger
                    retorno.color = colorDanger
                    break;
                case 'vazao':
                    if (valor > 10000) {
                        break;
                    }

                    if (valor > 5000) {
                        retorno.status = statusWarning
                        retorno.color = colorWarning
                        break;
                    }

                    retorno.status = statusDanger
                    retorno.color = colorDanger
                    break;
                case 'lucratividade':
                    if (valor > 10000) {
                        break;
                    }

                    if (valor > 5000) {
                        retorno.status = statusWarning
                        retorno.color = colorWarning
                        break;
                    }

                    retorno.status = statusDanger
                    retorno.color = colorDanger
                    break;
            }

            return retorno
        }

        function totalizarDados() {
            var projetado = 0
            var custo = 0
            var vazao = 0
            var lucratividade = 0

            for (var i = 0; i < vm.dadosDias.length; i++) {
                projetado += vm.dadosDias[i].projetado.valor
                custo += vm.dadosDias[i].custo.valor
                vazao += vm.dadosDias[i].vazao.valor
                lucratividade += vm.dadosDias[i].lucratividade.valor
            }

            vm.dadosTotal = {
                projetado: avaliarMetrica('projetado', projetado),
                custo: avaliarMetrica('custo', custo),
                vazao: avaliarMetrica('vazao', vazao),
                lucratividade: avaliarMetrica('lucratividade', lucratividade),
            }
        }



        ////////////////

        activate();


    }

})();