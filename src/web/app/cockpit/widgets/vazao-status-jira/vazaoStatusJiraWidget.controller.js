(function() {
    'use strict';

    angular
        .module('app.cockpit')
        .controller('vazaoStatusJiraWidgetController', vazaoStatusJiraWidgetController);

    vazaoStatusJiraWidgetController.$inject = ['$scope', 'demandasJiraService'];

    function vazaoStatusJiraWidgetController($scope, demandasJiraService) {
        var vm = this;
        vm.valor = 0;
        var widgetOptions = $scope.widget.options


        activate();

        ////////////////

        function activate() {
            setTimeout(function() {
                atualizarDemandas()
            }, 1000)

            setInterval(atualizarDemandas, 30 * 1000)
        }



        function atualizarDemandas() {

            Promise.all(
                    [
                        demandasJiraService
                        .listaDemandas(
                            moment().startOf('day').toDate(),
                            moment().endOf('day').toDate(),
                            'status',
                            widgetOptions.projeto
                        ),
                        demandasJiraService
                        .listaDemandas(
                            moment().subtract(1, 'day').startOf('day').toDate(),
                            moment().subtract(1, 'day').endOf('day').toDate(),
                            'status',
                            widgetOptions.projeto
                        )
                    ]
                )

                .then(function(promises) {
                    var anterior = 0
                    var atual = 0

                    _.forEach(promises[0], function(linha) {
                        if (!widgetOptions.status || _.indexOf(widgetOptions.status, linha.status) != -1) {
                            atual += linha.demandas
                        }
                    })

                    _.forEach(promises[1], function(linha) {
                        if (!widgetOptions.status || _.indexOf(widgetOptions.status, linha.status) != -1) {
                            anterior += linha.demandas
                        }
                    })

                    vm.valor = atual - anterior



                })


        }

    }

})();