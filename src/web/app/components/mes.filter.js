angular
        .module('app')
        .filter("mes", mes);


function mes() {
    return function (input) {
        var strMeses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

        if (input > 12 || input < 1) {
            return false;
        }

        return strMeses[input - 1];

    }
}
;

