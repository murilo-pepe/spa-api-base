(function () {
    'use strict';

    var core = angular.module('app.core');

    toastrConfig.$inject = ['toastr'];

    core.config(toastrConfig);

    function toastrConfig(toastr) {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "100",
            "timeOut": "5000",
            "extendedTimeOut": "5000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "preventDuplicates": true
            
        };
    }

    var config = {
        appErrorPrefix: '[PG+ Error] ',
        appTitle: 'PG+ Cockpit',
        version: '1.0.0'
    };

    core.value('config', config);

    configure.$inject = [
        '$logProvider',
        '$routeProvider',
        'routehelperConfigProvider',
        'exceptionHandlerProvider'

    ];

    core.config(configure);

    function configure($logProvider, $routeProvider, routehelperConfigProvider, exceptionHandlerProvider) {
        // turn debugging off/on (no info or warn)
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }

        // Configure the common route provider
        routehelperConfigProvider.config.$routeProvider = $routeProvider;
        routehelperConfigProvider.config.docTitle = 'PG+ Cockpit: ';


        // Configure the common exception handler
        exceptionHandlerProvider.configure(config.appErrorPrefix);

    }


})();
