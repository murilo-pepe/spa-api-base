(function () {
    'use strict';

    angular
            .module('app.core')
            .constant('toastr', toastr)
            .constant('api', {
                url: 'http://33.3.3.23:4040/'
            });
})();
