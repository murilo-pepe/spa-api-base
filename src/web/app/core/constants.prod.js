(function () {
    'use strict';

    angular
            .module('app.core')
            .constant('toastr', toastr)
            .constant('api', {
                url: 'http://cockpit.pgd.io:4040/'
            });
})();
