(function () {
    'use strict';

    angular
            .module('app.core')
            .controller("login", login);

    login.$inject = ["authService", "$location", "logger", '$routeParams'];

    function login(authService, $location, logger, $routeParams) {
        var vm = this;

        vm.nome = '';
        vm.email = '';
        vm.senha = '';
        vm.lembrar = false;

        vm.login = function () {
            authService
                    .login(vm.email, vm.senha)
                    .then(function () {
                        $location.path('/');
                    })
                    .catch(function (err) {
                        logger.error(err.message);
                    });
        };

        activate();

        function activate() {
            authService.limparCredenciais();
            if (typeof $routeParams.token != 'undefined') {
                authService
                        .loginViaToken($routeParams.token)
                        .then(function () {
                            $location.path('/');
                        })
                        .catch(function (err) {
                            logger.error(err.message);
                            $location.path('/login');
                        });
            }
        }

    }


})();