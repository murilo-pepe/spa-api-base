(function () {
    'use strict';

    angular
            .module('app.core')
            .controller("logout", logout);

    logout.$inject = ["authService", "$location", "logger", '$routeParams', '$rootScope'];

    function logout(authService, $location, logger, $routeParams, $rootScope) {
        var vm = this;

        activate();

        function activate() {
            $rootScope.blur = false
            authService.limparCredenciais();
            $location.path('/login');
        }

    }


})();