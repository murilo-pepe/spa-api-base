(function () {
    'use strict';
    angular
            .module('app.core')
            .run(appRun);

    appRun.$inject = ['routehelper', 'authService', '$location'];

    function appRun(routehelper, authService, $location) {
        if (!authService.restaurarCredenciais()) {
            $location.path('/login');
        }
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/login',
                config: {
                    templateUrl: 'app/core/views/login.html',
                    controller: 'login',
                    controllerAs: 'vm',
                    title: 'Login',
                    cleanLayout: true,
                }
            },
            {
                url: '/login/:token',
                config: {
                    templateUrl: 'app/core/views/empty.html',
                    controller: 'login',
                    controllerAs: 'vm',
                    title: 'Login',
                    cleanLayout: true,
                }
            },
            {
                url: '/logout',
                config: {
                    templateUrl: 'app/core/views/empty.html',
                    controller: 'logout',
                    controllerAs: 'vm',
                    title: 'Logout',
                    cleanLayout: true,
                }
            }
        ];
    }
})();

