(function() {
    'use strict';

    angular
        .module('app.core')
        .directive('autohide', ckpAutohide);

    ckpAutohide.$inject = ['$rootScope'];

    function ckpAutohide($rootScope) {
        function link(scope, el) {
            scope.visible = false;
            scope.hideEvent = null

            el.bind('mouseenter', function() {
                $rootScope.blur = true
                scope.visible = true;
                scope.$apply()

                clearTimeout(scope.hideEvent)
            });

            el.bind('mouseleave', function() {
                scope.hideEvent = setTimeout(function() {
                    $rootScope.blur = false
                    scope.visible = false;
                    scope.$apply()

                }, 500)

            });
        }

        return {
            restrict: 'EA',
            replace: true,
            transclude: 'element',
            template: "<div>" +
                "<div ng-transclude ng-show='visible'></div>" +
                "<div ng-show='!visible' class='navbar-fixed-top' style='height: 60px;'> </div>" +
                "</div>",
            link: link
        }
    }

})();