var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var cleancss = require('gulp-clean-css');
var gutil = require('gulp-util');
var htmlmin = require('gulp-htmlmin');
var versionAppend = require('gulp-version-append');
var rename = require('gulp-rename');
var inject = require('gulp-inject');
var sequence = require('run-sequence');
var flatten = require('gulp-flatten')

var env = process.env.NODE_ENV || 'dev';

console.log(' - Compilando para ambiente', gutil.colors.green(env));

if (env == 'dev') {
    console.log('Arquivos não serão minificados')
}


var jsFiles = [
    "bower_components/jquery/dist/jquery.js",
    "bower_components/angular/angular.js",
    "bower_components/angular-route/angular-route.js",
    "bower_components/angular-messages/angular-messages.js",
    "bower_components/angular-sanitize/angular-sanitize.js",
    "bower_components/extras.angular.plus/ngplus-overlay.js",
    "bower_components/toastr/toastr.js",
    "bower_components/lodash/lodash.js",
    "bower_components/moment/moment.js",
    "bower_components/angular-input-masks/angular-input-masks-standalone.min.js",
    "bower_components/angular-aria/angular-aria.js",
    "bower_components/angular-animate/angular-animate.js",
    "bower_components/angular-ui-router/release/angular-ui-router.min.js",
    "bower_components/angular-cookies/angular-cookies.js",
    "bower_components/angular-smart-table/dist/smart-table.min.js",
    "bower_components/angular-bootstrap/ui-bootstrap.js",
    "bower_components/bootstrap/dist/bootstrap.js",
    "bower_components/bootstrap/js/dropdown.js",
    "bower_components/bootstrap/js/transitions.js",
    "bower_components/bootstrap/js/collapse.js",
    "bower_components/angular-bootstrap/ui-bootstrap-tpls.js",
    "bower_components/javascript-detect-element-resize/jquery.resize.js",
    "bower_components/angular-gridster/dist/angular-gridster.min.js",
    "bower_components/bigtext/dist/bigtext.js",


    "app/app.module.js",


    "app/blocks/exception/exception.module.js",
    "app/blocks/exception/exception-handler.provider.js",
    "app/blocks/exception/exception.js",
    "app/blocks/logger/logger.module.js",
    "app/blocks/logger/logger.js",
    "app/blocks/router/router.module.js",
    "app/blocks/router/routehelper.js",
    "app/blocks/auth/auth.module.js",
    "app/blocks/auth/auth.service.js",


    "app/core/locale.js",
    "app/core/core.module.js",
    "app/core/constants.js",
    "app/core/constants." + env + ".js",
    "app/core/config.js",
    "app/core/core.routes.js",
    "app/core/controllers/login.controller.js",
    "app/core/controllers/logout.controller.js",
    "app/core/controllers/painelUsuario.controller.js",


    "app/core/directives/autohide.directive.js",


    "app/cockpit/cockpit.module.js",
    "app/cockpit/cockpit.routes.js",
    "app/cockpit/cockpit.run.js",


    "app/cockpit/filters/secondsToTime.filter.js",
    "app/cockpit/filters/numberToPower.filter.js",


    "app/cockpit/services/database-status.service.js",
    "app/cockpit/services/lancamento-horas.service.js",
    "app/cockpit/services/indice-horas-projetos.service.js",
    "app/cockpit/services/indicadores-sms.service.js",
    "app/cockpit/services/indicadores-email.service.js",
    "app/cockpit/services/indicadores-voz.service.js",
    "app/cockpit/services/demandas-jira.service.js",


    "app/cockpit/controllers/cockpitStatusBanco.controller.js",
    "app/cockpit/controllers/cockpitLancamentoHoras.controller.js",
    "app/cockpit/controllers/cockpitIndex.controller.js",
    "app/cockpit/controllers/cockpitIndiceHorasProjetos.controller.js",
    "app/cockpit/controllers/cockpitIndicadoresSms.controller.js",
    "app/cockpit/controllers/cockpitIndicadoresEmail.controller.js",
    "app/cockpit/controllers/cockpitIndicadoresVoz.controller.js",
    "app/cockpit/controllers/cockpitFaturamentoClientes.controller.js",
    "app/cockpit/controllers/cockpitGridDinamico.controller.js",
    "app/cockpit/controllers/cockpitFinanceiroGsm.controller.js",


    "app/cockpit/widgets/fila-demandas-jira/filaDemandasJiraWidget.controller.js",
    "app/cockpit/widgets/indicador-status-jira/indicadorStatusJiraWidget.controller.js",
    "app/cockpit/widgets/indicador-atraso-jira/indicadorAtrasoJiraWidget.controller.js",
    "app/cockpit/widgets/vazao-status-jira/vazaoStatusJiraWidget.controller.js",
    "app/cockpit/widgets/ragTimelineReport/ragTimelineReportWidget.controller.js",

];

var cssFiles = [
    "bower_components/components-font-awesome/css/font-awesome.min.css",
    "bower_components/toastr/toastr.css",
    "bower_components/angular-bootstrap/ui-bootstrap-csp.css",
    "bower_components/angular-gridster/dist/angular-gridster.min.css",
    "assets/css/smart-tables.css",
    "assets/css/bootstrap-darkly.css",
    "assets/css/animations.css",
    "assets/css/robotoFont.css",
    "assets/css/custom.css",
];

var pastaDestino = 'dist';

gulp.task('inject', function () {
    var target = gulp.src('./index.html');
    var sources = gulp.src(['dist/scripts.js', 'dist/styles.css'], {
        read: false,
    });

    return target
        .pipe(inject(sources, {
            ignorePath: pastaDestino,
            removeTags: true,
            transform: function (filepath) {
                arguments[0] = filepath + '?v=' + Math.floor(Math.random() * 123456);
                return inject.transform.apply(inject.transform, arguments);
            }
        }))
        .pipe(gulp.dest(pastaDestino));
});

gulp.task('min-js', function () {
    return gulp
        .src(jsFiles)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(pastaDestino))
        .pipe(rename('uglify.js'))
        .pipe(uglify())
        .pipe(gulp.dest(pastaDestino));

});

gulp.task('min-css', function () {
    return gulp
        .src(cssFiles)
        .pipe(concat('styles.css'))
        .pipe(cleancss())
        .pipe(gulp.dest(pastaDestino));

});

gulp.task('min-views', function () {
    return gulp
        .src('app/**/*.html', {
            base: './app/'
        })
        .pipe(htmlmin({
            collapseWhitespace: true
        }))
        .pipe(gulp.dest('./dist/app'));
});

gulp.task('min-index', function () {
    return gulp.src('index_dist.html', {
            base: './'
        })
        .pipe(versionAppend(['html', 'js', 'css'], {
            appendType: 'timestamp'
        }))
        .pipe(htmlmin({
            collapseWhitespace: true
        }))
        .pipe(rename('index.html'))
        .pipe(gulp.dest('dist'));
});

gulp.task('move-img', function () {
    return gulp.src([
            'assets/**/*.gif',
            'assets/**/*.svg',
            'assets/**/*.bmp',
            'assets/**/*.png'
        ], {
            base: './app/'
        })
        .pipe(gulp.dest(pastaDestino + '/assets'));
});

gulp.task('move-fonts', function () {
    return gulp.src([
            'bower_components/components-font-awesome/fonts/*.*',
            'bower_components/bootstrap/fonts/*.*',
        ], {
            base: './bower_components/components-font-awesome/fonts'
        })
        .pipe(flatten())
        .pipe(gulp.dest(pastaDestino + '/fonts'));
});

gulp.task('watch', function () {

    gulp.watch('./app/**/*.js', ['scripts', 'inject']);
    gulp.watch('./assets/**/*.css', ['styles', 'inject']);
    gulp.watch('./app/**/*.html', ['min-views']);
    gulp.watch('./index.html', ['min-index', 'inject']);

});


gulp.task('scripts', ['min-js']);
gulp.task('styles', ['min-css']);
gulp.task('html', ['min-views', 'min-index']);
gulp.task('assets', ['move-img', 'move-fonts']);

gulp.task('default', function (done) {
    sequence(
        'scripts',
        'styles',
        'html',
        'assets',
        'inject',
        done
    );
});